package by.weekmenu.adminka.ui.Events;

import by.weekmenu.adminka.ui.views.ingredient.IngredientPriceEditor;
import com.vaadin.flow.component.ComponentEvent;

public class DeletePriceRowEvent extends ComponentEvent<IngredientPriceEditor> {

    public DeletePriceRowEvent(IngredientPriceEditor component) {
        super(component, false);
    }
}
