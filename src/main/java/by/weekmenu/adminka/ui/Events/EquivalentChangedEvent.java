package by.weekmenu.adminka.ui.Events;


import by.weekmenu.adminka.ui.views.ingredient.IngredientUOMEditor;
import com.vaadin.flow.component.ComponentEvent;

public class EquivalentChangedEvent extends ComponentEvent<IngredientUOMEditor> {

    private final String equivalent;

    public EquivalentChangedEvent(IngredientUOMEditor source, String equivalent) {
        super(source, false);
        this.equivalent = equivalent;
    }

    public String getEquivalent() {
        return equivalent;
    }
}
