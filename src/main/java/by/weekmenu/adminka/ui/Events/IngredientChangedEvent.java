package by.weekmenu.adminka.ui.Events;

import by.weekmenu.adminka.ui.views.ingredient.IngredientPriceEditor;
import com.vaadin.flow.component.ComponentEvent;

public class IngredientChangedEvent extends ComponentEvent<IngredientPriceEditor> {

    public IngredientChangedEvent(IngredientPriceEditor component) {
        super(component, false);
    }
}
