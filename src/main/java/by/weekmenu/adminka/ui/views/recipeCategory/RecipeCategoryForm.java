package by.weekmenu.adminka.ui.views.recipeCategory;

import by.weekmenu.adminka.dto.RecipeCategoryDTO;
import by.weekmenu.adminka.service.RecipeCategoryService;
import by.weekmenu.adminka.ui.components.FormButtonsBar;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import java.util.List;
import java.util.stream.Collectors;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RecipeCategoryForm extends VerticalLayout implements CrudForm<RecipeCategoryDTO, Long> {

    private final TextField name;
    private final TextField priority;
    private final FormButtonsBar buttons;
    private RecipeCategoryDTO recipeCategoryDTO;
    private List<RecipeCategoryDTO> recipeCategoryDTOList;

    public RecipeCategoryForm(RecipeCategoryService recipeCategoryService) {
        setId("RecipeCategoryForm");
        name = new TextField("Категория рецепта");
        name.setAutofocus(true);
        name.setId("RecipeCategoryNameField");

        priority = new TextField("Приоритет категории");
        priority.setId("RecipeCategoryPriorityField");

        recipeCategoryDTOList = recipeCategoryService.getAllDTOs();

        buttons = new FormButtonsBar();
        add(name, priority, buttons);
    }

    @Override
    public FormButtonsBar getButtons() {
        return buttons;
    }

    @Override
    public void setBinder(Binder<RecipeCategoryDTO> binder, RecipeCategoryDTO entity) {
        this.recipeCategoryDTO = entity;
        binder.forField(name)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length() > 0, "Поле не может быть пустым")
                .withValidator(field -> field.length() <= 255, "Не более 255 символов")
                .withValidator(field -> {
                    if (name.getValue().equals(recipeCategoryDTO.getName())) {
                        return true;
                    } else {
                        return recipeCategoryDTOList.stream()
                                .filter(recipeCategoryDTO ->
                                        recipeCategoryDTO.getName().toUpperCase().equals(name.getValue().toUpperCase()))
                                .allMatch(recipeCategoryDTO -> recipeCategoryDTO == null);
                    }
                }, "Данное название категории рецепта уже внесено в базу")
                .bind(RecipeCategoryDTO::getName, RecipeCategoryDTO::setName);
        binder.forField(priority)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> {
                    try {
                        return Integer.parseInt(field) > 0;
                    } catch (Exception e) {
                        return false;
                    }
                }, "Приоритет должен быть целым положительным числом")
                .withValidator(field -> {
                    if (priority.getValue().equals(recipeCategoryDTO.getPriority())) {
                        return true;
                    } else {
                        List<RecipeCategoryDTO> recipeCategoryDTOs = recipeCategoryDTOList.stream()
                                .filter(recipeCategoryDTO ->
                                        (!(recipeCategoryDTO.getPriority() == null)))
                                .filter(recipeCategoryDTO ->
                                        recipeCategoryDTO.getPriority().equals(priority.getValue()))
                                .collect(Collectors.toList());
                        if (recipeCategoryDTOs.size() == 0) {
                            return true;
                        } else {
                            Notification.show("Это значение соответствует категории рецепта : " +
                                    recipeCategoryDTOs.get(0).getName());
                            return false;
                        }
                    }
                }, "Данный приоритет категории рецепта уже внесен в базу")
                .bind(RecipeCategoryDTO::getPriority, RecipeCategoryDTO::setPriority);
    }

    @Override
    public RecipeCategoryDTO getDTO() {
        return recipeCategoryDTO;
    }

    @Override
    public String getChangedDTOName() {
        return recipeCategoryDTO.getName();
    }

    @Override
    public Long getDTOId() {
        return recipeCategoryDTO.getId();
    }
}
