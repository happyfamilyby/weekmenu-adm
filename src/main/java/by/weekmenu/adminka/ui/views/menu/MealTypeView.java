package by.weekmenu.adminka.ui.views.menu;

import by.weekmenu.adminka.dto.MealTypeDTO;
import by.weekmenu.adminka.service.MealTypeService;
import by.weekmenu.adminka.ui.MainView;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.views.CrudForm;
import by.weekmenu.adminka.ui.views.CrudView;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = AppConsts.PAGE_MEAL_TYPE, layout = MainView.class)
@PageTitle(AppConsts.TITLE_MEAL_TYPE)
public class MealTypeView extends CrudView<MealTypeDTO, Short> {

    private final Binder<MealTypeDTO> binder;

    @Autowired
    public MealTypeView(CrudForm<MealTypeDTO, Short> form, MealTypeService serviceClient) {
        super(form, serviceClient);
        binder = new Binder<>(MealTypeDTO.class);
        if (serviceClient.getAllDTOs() != null) {
            getGrid().setItems(serviceClient.getAllDTOs());
        }
        getSearchBar().getSearchField().setVisible(false);
    }

    @Override
    public Binder<MealTypeDTO> getBinder() {
        return binder;
    }

    @Override
    protected void setupGrid() {
        getGrid().addColumn(MealTypeDTO::getName).setHeader("Приём пищи");
        getGrid().addColumn(MealTypeDTO::getPriority).setHeader("Приоритет приёма пищи");
    }
}
