package by.weekmenu.adminka.ui.views.menu;

import by.weekmenu.adminka.dto.MenuCategoryDTO;
import by.weekmenu.adminka.service.MenuCategoryService;
import by.weekmenu.adminka.ui.MainView;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.views.CrudForm;
import by.weekmenu.adminka.ui.views.CrudView;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = AppConsts.PAGE_MENUCATEGORY, layout = MainView.class)
@PageTitle(AppConsts.TITLE_MENUCATEGORY)
public class MenuCategoryView extends CrudView<MenuCategoryDTO, Integer> {

    private final Binder<MenuCategoryDTO> binder;

    @Autowired
    public MenuCategoryView(CrudForm<MenuCategoryDTO, Integer> form, MenuCategoryService serviceClient) {
        super(form, serviceClient);
        binder = new Binder<>(MenuCategoryDTO.class);
        if (serviceClient.getAllDTOs() != null) {
            getGrid().setItems(serviceClient.getAllDTOs());
        }
        getSearchBar().getSearchField().setVisible(false);
    }

    @Override
    public Binder<MenuCategoryDTO> getBinder() {
        return binder;
    }

    @Override
    protected void setupGrid() {
        getGrid().addColumn(new ComponentRenderer<>(MenuCategoryDTO -> {
            if ((MenuCategoryDTO.getImageLink() != null) && !StringUtils.isEmpty(MenuCategoryDTO.getImageLink())) {
                Image image = new Image(MenuCategoryDTO.getImageLink(), MenuCategoryDTO.getName());
                image.addClassName("small-image");
                return image;
            } else {
                Image imageEmpty = new Image("", "No image");
                imageEmpty.addClassName("small-image");
                return imageEmpty;
            }
        }
        ))
                .setHeader("Фото");
        getGrid().addColumn(MenuCategoryDTO::getName).setHeader("Категория меню");
        getGrid().addColumn(MenuCategoryDTO::getPriority).setHeader("Приоритет категории меню");
    }
}
