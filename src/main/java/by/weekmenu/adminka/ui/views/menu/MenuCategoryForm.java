package by.weekmenu.adminka.ui.views.menu;

import by.weekmenu.adminka.dto.MenuCategoryDTO;
import by.weekmenu.adminka.service.MenuCategoryService;
import by.weekmenu.adminka.ui.components.FormButtonsBar;
import by.weekmenu.adminka.ui.components.ImageUpload;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import java.util.List;
import java.util.stream.Collectors;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MenuCategoryForm extends VerticalLayout implements CrudForm<MenuCategoryDTO, Integer> {

    private final TextField name;
    private final TextField priority;
    private final TextField imageLink;
    private final FormButtonsBar buttons;
    private MenuCategoryDTO menuCategoryDTO;
    private final MenuCategoryService menuCategoryService;
    private List<MenuCategoryDTO> menuCategoryDTOList;


    public MenuCategoryForm(@Value("${upload.location}") String uploadLocation,
                            @Value("${images.path.pattern}") String pathPattern,
                            MenuCategoryService menuCategoryService) {
        this.menuCategoryService = menuCategoryService;
        setSizeFull();
        setId("MenuCategoryForm");
        name = new TextField("Категория меню");
        name.setAutofocus(true);
        name.setId("MenuCategoryNameField");

        priority = new TextField("Приоритет категории");
        priority.setId("MenuCategoryPriorityField");

        menuCategoryDTOList = menuCategoryService.getAllDTOs();
        imageLink = new TextField("Изображение");
        imageLink.setId("MenuCategoryImageLinkField");
        ImageUpload imageUpload = new ImageUpload(uploadLocation, pathPattern);
        imageUpload.uploadImage("MenuCategories", imageLink);
        imageUpload.setAcceptedFileTypes("image/jpeg", "image/png", "image/gif");
        HorizontalLayout choosePic = new HorizontalLayout(imageLink, imageUpload);
        choosePic.setSizeFull();

        buttons = new FormButtonsBar();
        add(name, priority, choosePic, buttons);
    }

    @Override
    public FormButtonsBar getButtons() {
        return buttons;
    }

    @Override
    public void setBinder(Binder<MenuCategoryDTO> binder, MenuCategoryDTO entity) {
        this.menuCategoryDTO = entity;
        binder.forField(name)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length() > 0, "Поле не может быть пустым")
                .withValidator(field -> field.length() <= 255, "Не более 255 символов")
                .withValidator(field -> {
                    if (name.getValue().equals(menuCategoryDTO.getName())) {
                        return true;
                    } else {
                        return menuCategoryDTOList.stream()
                                .filter(menuCategoryDTO -> menuCategoryDTO.getName().toUpperCase().equals(name.getValue().toUpperCase()))
                                .allMatch(menuCategoryDTO -> menuCategoryDTO == null);
                    }
                }, "Данное название категории меню уже внесено в базу")
                .bind(MenuCategoryDTO::getName, MenuCategoryDTO::setName);
        binder.forField(priority)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> {
                    try {
                        return Integer.parseInt(field) > 0;
                    } catch (Exception e) {
                        return false;
                    }
                }, "Приоритет должен быть целым положительным числом")
                .withValidator(field -> {
                    if (priority.getValue().equals(menuCategoryDTO.getPriority())) {
                        return true;
                    } else {
                        List<MenuCategoryDTO> menuCategoryDTOs = menuCategoryDTOList.stream()
                                .filter(menuCategoryDTO -> menuCategoryDTO.getPriority().equals(priority.getValue()))
                                .collect(Collectors.toList());
                        if (menuCategoryDTOs.size() == 0) {
                            return true;
                        } else {
                            Notification.show("Это значение соответствует категории меню : " +
                                    menuCategoryDTOs.get(0).getName());
                            return false;
                        }
                    }
                }, "Данный приоритет категории ингредиента уже внесен в базу")
                .bind(MenuCategoryDTO::getPriority, MenuCategoryDTO::setPriority);
        binder.forField(imageLink)
                .withValidator(field -> field.length() <= 255, "Не более 255 символов")
                .bind(MenuCategoryDTO::getImageLink, MenuCategoryDTO::setImageLink);
    }

    @Override
    public MenuCategoryDTO getDTO() {
        return menuCategoryDTO;
    }

    @Override
    public String getChangedDTOName() {
        return menuCategoryDTO.getName();
    }

    @Override
    public Integer getDTOId() {
        return menuCategoryDTO.getId();
    }
}
