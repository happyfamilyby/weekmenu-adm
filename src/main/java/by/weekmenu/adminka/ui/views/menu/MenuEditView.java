package by.weekmenu.adminka.ui.views.menu;

import by.weekmenu.adminka.dto.*;
import by.weekmenu.adminka.service.*;
import by.weekmenu.adminka.ui.components.FormButtonsBar;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.util.WeekMenuDatesUtils;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.Shortcuts;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.validator.StringLengthValidator;
import com.vaadin.flow.router.*;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.material.Material;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.vaadin.gatanaso.MultiselectComboBox;

import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.*;
import java.util.stream.Collectors;

import static by.weekmenu.adminka.ui.views.CrudView.errorNotification;

@Route(value = AppConsts.PAGE_MENU_EDIT + "/" + MenuEditView.TOKEN)
@PageTitle(AppConsts.TITLE_MENU_EDIT)
@Theme(Material.class)
@StyleSheet("frontend://styles/styles.css")
class MenuEditView extends SplitLayout implements HasUrlParameter<Long> {

    //Change value of TOKEN field on system variable TOKEN
    public final static String TOKEN = "BJKwXU0RjXyTLom";
    private final TextField menuName;
    private final ComboBox<String> menuCategoryName;
    private final MultiselectComboBox<String> mealTypeNames;
    private final Checkbox isActive;
    private final MenuAuthorInfo menuAuthorInfo;
    private HorizontalLayout header;
    private final VerticalLayout menuSection;
    private List<MenuGridRow> menuGridRows;
    private final TextField weekNumber;
    private final Binder<MenuDTO> binder = new Binder<>();
    private MenuDTO menuDTO;
    private final MenuService menuService;
    private final MealTypeService mealTypeService;
    private final RecipeService recipeService;
    private final FormButtonsBar buttonsBar;
    private Long id;

    public MenuEditView(@Value("${upload.location}") String uploadLocation,
                        @Value("${images.path.pattern}") String pathPattern,
                        RecipeService recipeService, MenuCategoryService menuCategoryService,
                        MenuService menuService, MealTypeService mealTypeService,
                        RecipeCategoryService recipeCategoryService, RecipeSubcategoryService recipeSubcategoryService) {
        this.menuService = menuService;
        this.mealTypeService = mealTypeService;
        this.recipeService = recipeService;
        addClassName("menu-edit-view");
        addToPrimary(new MenuEditViewRecipeFiltersLayout(recipeService, recipeCategoryService,
                recipeSubcategoryService));

        weekNumber = new TextField("№ недели");
        weekNumber.setId("MenuWeekNumberField");

        menuName = new TextField("Название меню");
        menuName.setWidth("400px");
        menuName.setId("MenuNameField");
        menuCategoryName = new ComboBox<>("Категория меню");
        menuCategoryName.setWidth("400px");
        menuCategoryName.setId("MenuCategoryNameField");
        menuCategoryName.setItems(menuCategoryService.getAllDTOs()
                .stream()
                .map(MenuCategoryDTO::getName)
                .collect(Collectors.toList()));
        menuCategoryName.addValueChangeListener(event -> {
            if (id == null) {
                if (!StringUtils.isEmpty(weekNumber.getValue()) && (event.getValue() != null)) {
                    menuName.setValue(event.getValue() + "  "
                            + WeekMenuDatesUtils.getWeekDates(Integer.parseInt(weekNumber.getValue())));
                }
                if (StringUtils.isEmpty(weekNumber.getValue()) && (event.getValue() != null)) {
                    menuName.setValue(event.getValue());
                }
            }
        });
        weekNumber.addValueChangeListener(event -> {
            if (id == null) {
                if (!StringUtils.isEmpty(event.getValue()) && (menuCategoryName.getValue() != null)) {
                    menuName.setValue(menuCategoryName.getValue() + "  "
                            + WeekMenuDatesUtils.getWeekDates(Integer.parseInt(event.getValue())));
                }
                if (!StringUtils.isEmpty(event.getValue()) && (menuCategoryName.getValue() == null)) {
                    menuName.setValue(WeekMenuDatesUtils.getWeekDates(Integer.parseInt(event.getValue())));
                }
            }
        });
        mealTypeNames = new MultiselectComboBox<>();
        mealTypeNames.setId("MealTypeNamesField");
        mealTypeNames.setWidth("600px");
        mealTypeNames.setLabel("Приёмы пищи");
        mealTypeNames.setItems(mealTypeService.getAllDTOs().stream()
                .sorted(Comparator.comparing(mealTypeDTO -> Integer.valueOf(mealTypeDTO.getPriority())))
                .map(MealTypeDTO::getName)
                .collect(Collectors.toList()));
        HorizontalLayout row1 = new HorizontalLayout(menuCategoryName, weekNumber);
        HorizontalLayout row2 = new HorizontalLayout(menuName, mealTypeNames);
        row1.addClassName("menu-identifiers-row1");
        row1.setWidthFull();
        row1.setJustifyContentMode(FlexComponent.JustifyContentMode.START);
        row2.addClassName("menu-row2");
        row2.setWidthFull();
        row2.setJustifyContentMode(FlexComponent.JustifyContentMode.START);
        isActive = new Checkbox("Активное");
        isActive.setId("MenuIsActiveBox");
        menuSection = new VerticalLayout();
        menuSection.addClassName("menu-section");
        mealTypeNames.addSelectionListener(e -> {
                    if (e.getRemovedSelection().size() > 0) {
                        Optional<MenuGridRow> row = menuGridRows.stream()
                                .filter(menuGridRow -> menuGridRow.getMealTypeNameButton().getText().equals(e.getRemovedSelection().iterator().next()))
                                .findAny();
                        row.ifPresent(menuGridRow -> {
                            menuGridRows.remove(menuGridRow);
                            if (menuGridRow.getParent().isPresent()) {
                                VerticalLayout parent = (VerticalLayout) menuGridRow.getParent().get();
                                menuSection.remove(parent);
                            }
                        });
                    } else if (e.getValue().size() > 0) {
                        if (header == null || !header.getParent().isPresent()) {
                            header = createDaysOfWeekHeader();
                            menuSection.add(header);
                        }
                        e.getAddedSelection().forEach(this::createMenuRow);
                    }
                    if (e.getValue().size() == 0) {
                        menuSection.removeAll();
                    }
                }
        );
        mealTypeNames.setValue(new HashSet<>(Arrays.asList("Завтрак", "Обед", "Ужин")));
        menuAuthorInfo = new MenuAuthorInfo(uploadLocation, pathPattern);
        buttonsBar = new FormButtonsBar();
        buttonsBar.addSaveListener(e -> save());
        buttonsBar.addDeleteListener(e -> delete());
        buttonsBar.addCancelListener(e -> cancel());
        Shortcuts.addShortcutListener(menuName,
                () -> UI.getCurrent().navigate(AppConsts.PAGE_MENU + "/" + TOKEN),
                Key.ESCAPE);
        addToSecondary(row1, row2, menuSection, menuAuthorInfo, isActive, buttonsBar);
        styling();
    }

    private void setupBinder(MenuDTO menuDTO) {
        if (menuDTO == null) {
            menuDTO = new MenuDTO();
        }
        this.menuDTO = menuDTO;
        binder.forField(weekNumber)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> {
                    try {
                        return (Integer.parseInt(field) > 0
                                && Integer.parseInt(field) <= WeekMenuDatesUtils.getNumberOfWeeksInTheYear());
                    } catch (Exception e) {
                        return false;
                    }
                }, "Приоритет должен быть целым положительным числом не превышающем значения "
                        + WeekMenuDatesUtils.getNumberOfWeeksInTheYear())
                .bind(MenuDTO::getWeekNumber, MenuDTO::setWeekNumber);
        binder.forField(menuName)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length() > 0, "Поле не может быть пустым")
                .withValidator(new StringLengthValidator("Не более 255 символов", 1, 255))
                .withValidator(field -> {
                    if (menuName.getValue().equals(this.menuDTO.getName())) {
                        return true;
                    } else {
                        Optional<MenuDTO> menuFromDB = menuService.getAllDTOs()
                                .stream()
                                .filter(menuDTO1 -> menuDTO1.getName().equalsIgnoreCase(field.trim()))
                                .findAny();
                        return !menuFromDB.isPresent();
                    }
                }, "Данное название меню уже внесено в базу")
                .bind(MenuDTO::getName, MenuDTO::setName);
        binder.forField(menuCategoryName)
                .asRequired("Поле не может быть пустым")
                .bind(MenuDTO::getMenuCategoryName, MenuDTO::setMenuCategoryName);
        binder.forField(isActive).bind(MenuDTO::isActive, MenuDTO::setActive);
        binder.forField(menuAuthorInfo.getImageLink())
                .withValidator(new StringLengthValidator("Не более 255 символов", 0, 255))
                .bind(MenuDTO::getAuthorImageLink, MenuDTO::setAuthorImageLink);
        binder.forField(menuAuthorInfo.getAuthorName())
                .withValidator(new StringLengthValidator("Не более 255 символов", 0, 255))
                .bind(MenuDTO::getAuthorName, MenuDTO::setAuthorName);
        binder.forField(menuAuthorInfo.getMenuDescription())
                .withValidator(new StringLengthValidator("Не более 1000 символов", 0, 1000))
                .bind(MenuDTO::getMenuDescription, MenuDTO::setMenuDescription);
        if (menuDTO.getMenuRecipeDTOS() != null) {
            Set<String> mealTypeNames = menuDTO.getMenuRecipeDTOS()
                    .stream()
                    .map(MenuRecipeDTO::getMealTypeName)
                    .collect(Collectors.toSet());
            this.mealTypeNames.setValue(mealTypeNames);
            Set<MenuRecipeDTO> menuRecipeDTOS = menuDTO.getMenuRecipeDTOS();
            if (menuGridRows != null && menuDTO.getId() != null) {
                menuGridRows.forEach(menuGridRow -> menuGridRow.createCard(menuRecipeDTOS));
            }
        }
        binder.readBean(menuDTO);
    }

    private void save() {
        try {
            binder.writeBean(menuDTO);
            if (menuGridRows != null) {
                Set<MenuRecipeDTO> menuRecipeDTOSet = new HashSet<>();
                menuGridRows.forEach(menuGridRow ->
                        menuRecipeDTOSet.addAll(menuGridRow.getMenuRecipeDTOS()));
                menuDTO.setMenuRecipeDTOS(menuRecipeDTOSet);
            }
            if (menuDTO.getId() == null) {
                MenuDTO saved = menuService.createDTO(menuDTO);
                if (saved != null) {
                    Notification.show("Меню '" + saved.getName() + "' сохранено");
                    UI.getCurrent().navigate(AppConsts.PAGE_MENU + "/" + TOKEN);
                }
            } else {
                menuService.updateDTO(menuDTO);
                Notification.show("Меню '" + menuDTO.getName() + "' обновлено");
                UI.getCurrent().navigate(AppConsts.PAGE_MENU + "/" + TOKEN);
            }
        } catch (ValidationException e) {
            errorNotification(e);
        }
    }

    private void delete() {
        try {
            menuService.deleteDTO(menuDTO);
            UI.getCurrent().navigate(AppConsts.PAGE_MENU + "/" + TOKEN);
        } catch (Exception e) {
            errorNotification(e);
        }
    }

    private void cancel() {
        UI.getCurrent().navigate(AppConsts.PAGE_MENU + "/" + TOKEN);
    }

    private void copyMenu(MenuDTO menuDTO) {
        UI.getCurrent()
                .navigate(AppConsts.PAGE_MENU_EDIT + "/" + TOKEN);
        this.menuDTO = menuDTO;
        menuDTO.setId(null);
        menuDTO.setName(menuDTO.getName() + " КОПИЯ");
        setupBinder(menuDTO);
        menuName.getElement().callFunction("scrollIntoView");
    }

    private void createMenuRow(String mealName) {
        MenuGridRow menuGridRow = new MenuGridRow();
        menuGridRow.createRow(mealName);
        if (menuGridRows == null) {
            menuGridRows = new ArrayList<>();
        }
        menuGridRows.add(menuGridRow);
        mealTypeService.getAllDTOs().stream().filter(mealTypeDTO -> mealTypeDTO.getName().equals(mealName)).findAny()
                .ifPresent(mealTypeDTO -> menuGridRow.setPriority(Integer.valueOf(mealTypeDTO.getPriority())));
        List<MenuGridRow> sortedRows = menuGridRows.stream()
                .sorted(Comparator.comparing(MenuGridRow::getPriority)).collect(Collectors.toList());
        sortedRows.forEach(gridRow -> {
            if (gridRow.getParent().isPresent()) {
                menuSection.remove(gridRow.getParent().get());
            }
            VerticalLayout buttonAndGridRow = new VerticalLayout();
            buttonAndGridRow.addComponentAsFirst(gridRow.getMealTypeNameButton());
            buttonAndGridRow.add(gridRow);
            menuSection.add(buttonAndGridRow);
        });
    }

    private HorizontalLayout createDaysOfWeekHeader() {
        Locale ruLocale = Locale.forLanguageTag("ru-RU");
        HorizontalLayout daysOfWeek = new HorizontalLayout();
        daysOfWeek.setId("days-header");
        for (int i = 1; i < 8; i++) {
            daysOfWeek.add(new Div(new Span(DayOfWeek.of(i).getDisplayName(TextStyle.SHORT, ruLocale))));
        }
        return daysOfWeek;
    }

    private void styling() {
        setSplitterPosition(20);
        setPrimaryStyle("width", "200px");
        setSizeFull();
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, @OptionalParameter Long id) {
        if (id == null) {
            this.id = null;
            setupBinder(null);
        } else {
            this.id = id;
            setupBinder(menuService.findMenuDTO(id));
            Button copyMenu = new Button("Копировать");
            copyMenu.addClickListener(e -> copyMenu(menuDTO));
            buttonsBar.add(copyMenu);
        }
    }
}
