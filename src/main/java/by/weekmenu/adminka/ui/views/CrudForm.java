package by.weekmenu.adminka.ui.views;

import by.weekmenu.adminka.ui.components.FormButtonsBar;
import com.vaadin.flow.data.binder.Binder;

public interface CrudForm<E, ID> {

    FormButtonsBar getButtons();

    void setBinder(Binder<E> binder, E entity);

    E getDTO();

    String getChangedDTOName();

    ID getDTOId();
}
