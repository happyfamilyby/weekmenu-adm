package by.weekmenu.adminka.ui.views.menu;

import by.weekmenu.adminka.ui.components.ImageUpload;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import org.apache.commons.lang3.StringUtils;

class MenuAuthorInfo extends VerticalLayout {

    private final TextField imageLink;
    private final TextField authorName;
    private final TextArea menuDescription;

    public MenuAuthorInfo(String uploadLocation, String pathPattern) {
        addClassName("menu-author-info");
        imageLink = new TextField();
        authorName = new TextField("Автор/партнер");
        authorName.setId("AuthorNameField");
        menuDescription = new TextArea("Описание меню");
        menuDescription.setId("MenuDescriptionArea");
        ImageUpload imageUpload = new ImageUpload(uploadLocation, pathPattern);
        imageUpload.uploadImage("Menus", imageLink);
        imageUpload.setAcceptedFileTypes("image/jpeg", "image/png", "image/gif");
        Image authorImage = new Image("", "");
        authorImage.setId("AuthorImage");
        HorizontalLayout upload = new HorizontalLayout(authorName, authorImage, imageUpload);
        upload.setWidthFull();
        upload.setJustifyContentMode(JustifyContentMode.EVENLY);
        upload.setDefaultVerticalComponentAlignment(Alignment.BASELINE);
        imageLink.addValueChangeListener(e -> {
            Image image = createImage(pathPattern);
            authorImage.setSrc(image.getSrc());
            image.getAlt().ifPresent(authorImage::setAlt);
        });
        upload.setVerticalComponentAlignment(Alignment.CENTER);
        add(upload, menuDescription);
        setHorizontalComponentAlignment(Alignment.CENTER, menuDescription);
    }

    private Image createImage(String pathPattern) {
        if (imageLink.getValue() != null && !StringUtils.isEmpty(imageLink.getValue())) {
            return new Image(imageLink.getValue(), authorName.getValue());
        } else {
            return new Image(pathPattern + "customProduct.png", "No image");
        }
    }

    public TextField getImageLink() {
        return imageLink;
    }

    public TextField getAuthorName() {
        return authorName;
    }

    public TextArea getMenuDescription() {
        return menuDescription;
    }
}
