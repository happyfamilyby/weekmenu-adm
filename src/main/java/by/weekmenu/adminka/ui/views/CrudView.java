package by.weekmenu.adminka.ui.views;

import by.weekmenu.adminka.dto.*;
import by.weekmenu.adminka.service.CrudServiceClient;
import by.weekmenu.adminka.service.SearchService;
import by.weekmenu.adminka.ui.components.SearchBar;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.NativeButton;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;

public abstract class CrudView<E, ID> extends VerticalLayout implements HasUrlParameter<String> {

    @Value("${token}")
    private String token;

    private final CrudServiceClient<E, ID> crudService;
    private SearchService<E> searchService;
    private final Dialog dialog = new Dialog();
    private final CrudForm<E, ID> form;
    private Grid<E> grid;
    private SearchBar searchBar;
    private H1 h1;

    protected CrudView(CrudForm<E, ID> form,
                       CrudServiceClient<E, ID> crudService) {
        this.form = form;
        this.crudService = crudService;

        setupView();
    }

    protected CrudView(CrudForm<E, ID> form,
                       CrudServiceClient<E, ID> crudService,
                       SearchService<E> searchService) {
        this.form = form;
        this.crudService = crudService;
        this.searchService = searchService;

        setupView();
    }

    private void setupView() {
        addComponents();

        setupGrid();
        updateList(null);
        gridSelectRowListener();

        //drag and drop columns order
        grid.setColumnReorderingAllowed(true);
        setupEventListeners();
    }

    private void gridSelectRowListener() {
        grid.asSingleSelect().addValueChangeListener(e -> {
            getForm().setBinder(getBinder(), e.getValue());
            Object value = e.getValue();
            if (value instanceof UnitOfMeasureDTO && ((UnitOfMeasureDTO) value).getFullName().equals("Грамм")) {
                getForm().getButtons().getDeleteButton().setVisible(false);
                getForm().getButtons().getSaveButton().setVisible(false);
                Notification.show("Грамм нельзя изменить");
            } else if ((value instanceof MealTypeDTO && ((MealTypeDTO) value).getName().equals("Завтрак")) ||
                    (value instanceof MealTypeDTO && ((MealTypeDTO) value).getName().equals("Обед")) ||
                    (value instanceof MealTypeDTO && ((MealTypeDTO) value).getName().equals("Ужин"))) {
                getForm().getButtons().getDeleteButton().setVisible(false);
                getForm().getButtons().getSaveButton().setVisible(false);
                Notification.show("Это значение нельзя изменить");
            } else {
                getForm().getButtons().getDeleteButton().setVisible(true);
                getForm().getButtons().getSaveButton().setVisible(true);
            }
            getBinder().readBean(e.getValue());
            dialog.open();
        });
    }

    private void addComponents() {
        setSizeFull();
        setDefaultHorizontalComponentAlignment(Alignment.CENTER);
        h1 = new H1("Пока ничего не добавлено");
        dialog.add((Component) getForm());
        searchBar = new SearchBar(this);
        grid = new Grid<>();
    }

    private void setupEventListeners() {
        getForm().getButtons().addSaveListener(e -> save());
        getForm().getButtons().addCancelListener(e -> cancel());
        getForm().getButtons().addDeleteListener(e -> delete());

        getDialog().getElement().addEventListener("opened-changed", e -> {
            if (!getDialog().isOpened()) {
                cancel();
            }
        });
    }

    protected void save() {
        E entityDTO = getForm().getDTO();
        boolean isValid = getBinder().writeBeanIfValid(entityDTO);
        if (isValid) {
            if (getForm().getDTOId() == null) {
                try {
                    E savedEntity = crudService.createDTO(entityDTO);
                    if (savedEntity != null) {
                        Notification.show(getForm().getChangedDTOName() + " сохранён");
                        closeUpdate();
                    }
                } catch (Exception e) {
                    errorNotification(e);
                }
            } else {
                try {
                    crudService.updateDTO(entityDTO);
                    Notification.show(getForm().getChangedDTOName() + " обновлён");
                    closeUpdate();
                } catch (Exception e) {
                    errorNotification(e);
                }

            }
        }
    }

    public static void errorNotification(Exception e) {
        e.printStackTrace();
        NativeButton button = new NativeButton("Закрыть");
        Label error = new Label(e.toString());
        Notification notification = new Notification(error, button);
        notification.setPosition(Notification.Position.TOP_STRETCH);
        notification.open();
        button.addClickListener(event -> notification.close());
    }

    private void cancel() {
        getGrid().asSingleSelect().clear();
        getDialog().close();
        updateList(null);
        getGrid().getDataProvider().refreshAll();
    }

    private void delete() {
        E entityDTO = getForm().getDTO();
        try {
            crudService.deleteDTO(entityDTO);
            closeUpdate();
        } catch (Exception e) {
            errorNotification(e);
        }
    }

    protected void closeUpdate() {
        getGrid().asSingleSelect().clear();
        getDialog().close();
        updateList(null);
        grid.getDataProvider().refreshAll();
    }

    public void updateList(String search) {
        if (StringUtils.isEmpty(search)) {
            List<E> list = crudService.getAllDTOs();
            if (list.size() > 0) {
                grid.setItems(list);
                remove(h1);
                add(searchBar, grid);
                if ((list.get(0) instanceof IngredientDTO) || (list.get(0) instanceof RecipeDTO)) {
                    searchBar.getSearchField().setVisible(true);
                }
            } else {
                remove(grid);
                add(searchBar, h1);
                searchBar.getSearchField().setVisible(false);
            }
        } else {
            grid.setItems(searchService.searchName(search));
        }
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, String s) {
        if (!s.equals(token)) {
            throw new IllegalArgumentException("Введите верный токен безопасности");
        }
    }

    public abstract Binder<E> getBinder();

    protected abstract void setupGrid();

    public CrudForm<E, ID> getForm() {
        return form;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public Grid<E> getGrid() {
        return grid;
    }

    public CrudServiceClient<E, ID> getCrudService() {
        return crudService;
    }

    protected SearchBar getSearchBar() {
        return searchBar;
    }
}
