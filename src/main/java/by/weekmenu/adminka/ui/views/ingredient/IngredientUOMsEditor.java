package by.weekmenu.adminka.ui.views.ingredient;

import by.weekmenu.adminka.dto.IngredientDTO;
import by.weekmenu.adminka.service.UnitOfMeasureService;
import by.weekmenu.adminka.ui.beans.IngredientUOM;
import by.weekmenu.adminka.ui.util.Converters;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.HasValueAndElement;
import com.vaadin.flow.component.internal.AbstractFieldSupport;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.shared.Registration;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

class IngredientUOMsEditor extends VerticalLayout implements HasValueAndElement<AbstractField.ComponentValueChangeEvent<IngredientUOMsEditor, List<IngredientUOM>>, List<IngredientUOM>> {

    private final CrudForm<IngredientDTO, Long> form;
    private final UnitOfMeasureService unitOfMeasureService;
    private final AbstractFieldSupport<IngredientUOMsEditor,List<IngredientUOM>> fieldSupport;

    IngredientUOMsEditor(CrudForm<IngredientDTO, Long> form, UnitOfMeasureService unitOfMeasureService) {
        this.form = form;
        this.unitOfMeasureService = unitOfMeasureService;
        this.fieldSupport = new AbstractFieldSupport<>(this, Collections.emptyList(),
                Objects::equals, c ->  {});
        setSizeFull();
        setPadding(false);
        setMargin(false);
    }

    @Override
    public void setValue(List<IngredientUOM> items) {
        fieldSupport.setValue(items);
        removeAll();
        if (items!=null) {
            IngredientUOM gram = new IngredientUOM();
            Optional<IngredientUOM> gramFromDB = items.stream()
                    .filter(ingredientUOM -> ingredientUOM.getUnitOfMeasureFullName().equals("Грамм"))
                    .findFirst();
            if (gramFromDB.isPresent()) {
                gram = gramFromDB.get();
            }
            Collections.swap(items, 0, items.indexOf(gram));
            for (int i=0; i<items.size(); i++) {
                IngredientUOM ingredientUOM = items.get(i);
                ingredientUOM.setEquivalent(Converters.removeZeros(ingredientUOM.getEquivalent()));
                IngredientUOMEditor editor = createEditor(ingredientUOM);
                editor.getAddUOM().setVisible(false);
                if (i == 0) {
                    editor.getDeleteUOM().setVisible(false);
                }
                if (i == items.size()-1) {
                    editor.getAddUOM().setVisible(true);
                }
            }
            if (items.size()==0) {
                createEditor(null);
            }
        }
    }

    IngredientUOMEditor createEditor(IngredientUOM ingredientUOM) {
        IngredientUOMEditor editor = new IngredientUOMEditor(form, unitOfMeasureService, ingredientUOM, this);
        getElement().appendChild(editor.getElement());
        editor.addFullNameChangeListener(e -> checkChanges(e.getSource()));
        editor.addEquivalentChangeListener(e -> checkChanges(e.getSource()));
        editor.addDeleteListener(e -> {
                remove(e.getSource());
                deleteIngredientUOM(e.getSource().getIngredientUOM());
        });
        editor.setValue(ingredientUOM);
        if (form.getDTO()!=null && form.getDTO().getIngredientUOMS().size()==1) {
                editor.getDeleteUOM().setVisible(false);
        }
        return editor;
    }

    private void checkChanges(IngredientUOMEditor ingredientUOMEditor) {
        if (ingredientUOMEditor.getIngredientUOM()==null) {
                if (!ingredientUOMEditor.getFullNameField().isEmpty() && !ingredientUOMEditor.getEquivalentField().isEmpty()) {
                    if (ingredientUOMEditor.getBinder().validate().isOk()) {
                    ingredientUOMEditor.setIngredientUOMs();
                } else {
                        ingredientUOMEditor.getBinder().validate();
                    }
            }
        }
    }

    private void deleteIngredientUOM(IngredientUOM ingredientUOM) {
        if (form.getDTO().getIngredientUOMS()!=null) {
            setValue(form.getDTO().getIngredientUOMS().stream()
                    .filter(element -> element != ingredientUOM).collect(Collectors.toList()));
            form.getDTO().setIngredientUOMS(getValue());
        }
    }

    @Override
    public List<IngredientUOM> getValue() {
        return fieldSupport.getValue();
    }

    @Override
    public Registration addValueChangeListener(ValueChangeListener<? super AbstractField.ComponentValueChangeEvent<IngredientUOMsEditor, List<IngredientUOM>>> listener) {
        return fieldSupport.addValueChangeListener(listener);
    }
}
