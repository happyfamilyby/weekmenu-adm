package by.weekmenu.adminka.ui.views.recycleBin;

import by.weekmenu.adminka.dto.RecycleBinDTO;
import by.weekmenu.adminka.service.RecycleBinService;
import by.weekmenu.adminka.ui.MainView;
import by.weekmenu.adminka.ui.util.AppConsts;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.LocalDateTimeRenderer;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.claspina.confirmdialog.ButtonOption;
import org.claspina.confirmdialog.ConfirmDialog;
import org.springframework.beans.factory.annotation.Value;

import java.time.format.DateTimeFormatter;
import java.util.List;

@Route(value = AppConsts.PAGE_RECYCLE_BIN, layout = MainView.class)
@PageTitle(AppConsts.TITLE_RECYCLE_BIN)
public class RecycleBinView extends VerticalLayout implements HasUrlParameter<String>{

    @Value("${token}")
    private String token;

    private final Grid<RecycleBinDTO> grid;
    private final H1 h1;
    private final Button deleteAllButton;
    private final RecycleBinService recycleBinService;

    public RecycleBinView(RecycleBinService recycleBinService) {
        this.recycleBinService = recycleBinService;
        setSizeFull();
        setDefaultHorizontalComponentAlignment(Alignment.END);
        grid = new Grid<>();
        h1 = new H1("Пока корзина пуста");
        setHorizontalComponentAlignment(Alignment.CENTER, h1);
        setupGrid();
        deleteAllButton = createDeleteAllButton(grid);
        deleteAllButton.setId("DeleteAllButton");
        add(deleteAllButton);
        updateView();
    }

    private void updateView() {
        List<RecycleBinDTO> allBins = recycleBinService.getAllBins();
        if (allBins.size()>0) {
            remove(h1);
            add(grid);
            grid.setItems(recycleBinService.getAllBins());
            deleteAllButton.setEnabled(true);
        } else {
            add(h1);
            remove(grid);
            deleteAllButton.setEnabled(false);
        }
    }

    private void setupGrid() {
        grid.addColumn(RecycleBinDTO::getElementName).setHeader("Элемент");
        grid.addColumn(RecycleBinDTO::getEntityName).setHeader("Таблица");
        grid.addColumn(new LocalDateTimeRenderer<>(RecycleBinDTO::getDeleteDate,
                DateTimeFormatter.ofPattern("dd.MM.yyy HH:mm")))
                .setHeader("Дата удаления");
        grid.addComponentColumn(item ->
                new HorizontalLayout(createDeleteButton(grid, item), createRestoreButton(grid, item)))
                .setHeader("Действия").setFlexGrow(2);
    }

    private Button createDeleteButton(Grid<RecycleBinDTO> grid, RecycleBinDTO recycleBinDTO) {
        Button deleteButton = new Button("Удалить", click -> ConfirmDialog.createWarning()
                .withCaption("Удалить навсегда?")
                .withMessage("Внимание: данное действие нельзя отменить.")
                .withOkButton(() -> {
                    recycleBinService.delete(recycleBinDTO.getId());
                    updateView();
                    Notification.show(recycleBinDTO.getElementName() + " удалён");
                }, ButtonOption.caption("Удалить"))
                .withCancelButton(ButtonOption.caption("Отменить"))
                .open());
        deleteButton.setId("RecycleBinDeleteButton");
        return deleteButton;
    }

    private Button createRestoreButton(Grid<RecycleBinDTO> grid, RecycleBinDTO recycleBinDTO) {
        Button restoreButton = new Button("Восстановить", click -> {
            recycleBinService.restore(recycleBinDTO.getId());
            Notification.show(recycleBinDTO.getElementName() + " восстановлен");
            updateView();
        });
        restoreButton.setId("RecycleBinRestoreButton");
        return restoreButton;
    }

    private Button createDeleteAllButton(Grid<RecycleBinDTO> grid) {
        Button deleteAllButton = new Button("Удалить всё", click -> ConfirmDialog.createWarning()
                .withCaption("Полностью очистить корзину?")
                .withMessage("Внимание: данное действие нельзя отменить.")
                .withOkButton(() -> {
                    for (RecycleBinDTO recycleBinDTO : recycleBinService.getAllBins()) {
                        recycleBinService.delete(recycleBinDTO.getId());
                    }
                    updateView();
                    Notification.show("Корзина очищена.");
                }, ButtonOption.caption("Удалить все"))
                .withCancelButton(ButtonOption.caption("Отменить"))
                .open());
        deleteAllButton.setId("RecycleBinDeleteAllButton");
        deleteAllButton.addThemeVariants(ButtonVariant.MATERIAL_CONTAINED);
        return deleteAllButton;
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, String s) {
        if (!s.equals(token)) {
            throw new IllegalArgumentException();
        }
    }
}
