package by.weekmenu.adminka.ui.views.recipeCategory;

import by.weekmenu.adminka.dto.RecipeSubcategoryDTO;
import by.weekmenu.adminka.service.RecipeSubcategoryService;
import by.weekmenu.adminka.ui.components.FormButtonsBar;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import java.util.List;
import java.util.stream.Collectors;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RecipeSubcategoryForm extends VerticalLayout implements CrudForm<RecipeSubcategoryDTO, Long> {

    private final TextField name;
    private final TextField priority;
    private final FormButtonsBar buttons;
    private RecipeSubcategoryDTO recipeSubcategoryDTO;
    private List<RecipeSubcategoryDTO> recipeSubcategoryDTOList;

    public RecipeSubcategoryForm(RecipeSubcategoryService recipeSubcategoryService) {
        setId("RecipeSubcategoryForm");
        name = new TextField("Подкатегория рецепта");
        name.setAutofocus(true);
        name.setId("RecipeSubcategoryNameField");

        priority = new TextField("Приоритет подкатегории");
        priority.setId("RecipeSubcategoryPriorityField");

        recipeSubcategoryDTOList = recipeSubcategoryService.getAllDTOs();

        buttons = new FormButtonsBar();
        add(name, priority, buttons);
    }

    @Override
    public FormButtonsBar getButtons() {
        return buttons;
    }

    @Override
    public void setBinder(Binder<RecipeSubcategoryDTO> binder, RecipeSubcategoryDTO entity) {
        this.recipeSubcategoryDTO = entity;
        binder.forField(name)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length() > 0, "Поле не может быть пустым")
                .withValidator(field -> field.length() <= 255, "Не более 255 символов")
                .withValidator(field -> {
                    if (name.getValue().equals(recipeSubcategoryDTO.getName())) {
                        return true;
                    } else {
                        return recipeSubcategoryDTOList.stream()
                                .filter(recipeSubcategoryDTO ->
                                        recipeSubcategoryDTO.getName().toUpperCase().equals(name.getValue().toUpperCase()))
                                .allMatch(recipeSubcategoryDTO -> recipeSubcategoryDTO == null);
                    }
                }, "Данное название подкатегории рецепта уже внесено в базу")
                .bind(RecipeSubcategoryDTO::getName, RecipeSubcategoryDTO::setName);
        binder.forField(priority)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> {
                    try {
                        return Integer.parseInt(field) > 0;
                    } catch (Exception e) {
                        return false;
                    }
                }, "Приоритет должен быть целым положительным числом")
                .withValidator(field -> {
                    if (priority.getValue().equals(recipeSubcategoryDTO.getPriority())) {
                        return true;
                    } else {
                        List<RecipeSubcategoryDTO> recipeSubcategoryDTOs = recipeSubcategoryDTOList.stream()
                                .filter(recipeSubcategoryDTO ->
                                        (!(recipeSubcategoryDTO.getPriority() == null)))
                                .filter(recipeSubcategoryDTO ->
                                        recipeSubcategoryDTO.getPriority().equals(priority.getValue()))
                                .collect(Collectors.toList());
                        if (recipeSubcategoryDTOs.size() == 0) {
                            return true;
                        } else {
                            Notification.show("Это значение соответствует подкатегории рецепта : " +
                                    recipeSubcategoryDTOs.get(0).getName());
                            return false;
                        }
                    }
                }, "Данный приоритет подкатегории рецепта уже внесен в базу")
                .bind(RecipeSubcategoryDTO::getPriority, RecipeSubcategoryDTO::setPriority);
    }

    @Override
    public RecipeSubcategoryDTO getDTO() {
        return recipeSubcategoryDTO;
    }

    @Override
    public String getChangedDTOName() {
        return recipeSubcategoryDTO.getName();
    }

    @Override
    public Long getDTOId() {
        return recipeSubcategoryDTO.getId();
    }
}
 