package by.weekmenu.adminka.ui.views.menu;

import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.EqualsExclude;
import org.vaadin.stefan.dnd.drag.DragSourceExtension;

import java.time.DayOfWeek;
import java.util.Objects;

@Data
@EqualsAndHashCode(exclude = {"priority", "dayOfWeek", "mealTypeName", "iconRow", "imageLink"})
class RecipeCard extends VerticalLayout {

    private final String imageLink;
    private final String recipeName;
    private final HorizontalLayout iconRow;
    private final String mealTypeName;
    private DayOfWeek dayOfWeek;
    private Integer priority;

    public RecipeCard(String imageLink, String recipeName, String mealTypeName, DayOfWeek dayOfWeek, MenuGridRow menuGridRow) {
        this.imageLink = imageLink;
        this.recipeName = recipeName;
        this.mealTypeName = mealTypeName;
        this.dayOfWeek = dayOfWeek;
        addClassName("recipe-card");
        Image image = new Image(imageLink, recipeName);
        image.addClassName("small-image");
        Icon icon = new Icon(VaadinIcon.CLOSE_SMALL);
        iconRow = new HorizontalLayout(icon);
        iconRow.setId("Icon");
        icon.addClickListener(clickEvent -> {
            removeAll();
            menuGridRow.deleteMenuRecipeDTO(this);
        });
        add(iconRow, image, new Span(recipeName));
        DragSourceExtension<RecipeCard> dragSource = DragSourceExtension.extend(this);
    }

    public String getImageLink() {
        return imageLink;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public HorizontalLayout getIconRow() {
        return iconRow;
    }

    public String getMealTypeName() {
        return mealTypeName;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }
}
