package by.weekmenu.adminka.ui.views.ingredient;

import by.weekmenu.adminka.dto.IngredientCategoryDTO;
import by.weekmenu.adminka.service.IngredientCategoryService;
import by.weekmenu.adminka.ui.components.FormButtonsBar;
import by.weekmenu.adminka.ui.components.ImageUpload;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import java.util.List;
import java.util.stream.Collectors;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class IngredientCategoryForm extends VerticalLayout implements CrudForm<IngredientCategoryDTO, Integer> {

    private final TextField name;
    private final TextField priority;
    private final TextField imageLink;
    private final FormButtonsBar buttons;
    private IngredientCategoryDTO ingredientCategoryDTO;
    private final IngredientCategoryService ingredientCategoryService;
    private List<IngredientCategoryDTO> ingredientCategoryDTOList;

    public IngredientCategoryForm(@Value("${upload.location}") String uploadLocation,
                                  @Value("${images.path.pattern}") String pathPattern,
                                  IngredientCategoryService ingredientCategoryService) {
        this.ingredientCategoryService = ingredientCategoryService;
        setSizeFull();
        setId("IngredientCategoryForm");
        name = new TextField("Категория ингредиента");
        name.setAutofocus(true);
        name.setId("IngredientCategoryNameField");

        priority = new TextField("Приоритет категории");
        priority.setId("IngredientCategoryPriorityField");

        ingredientCategoryDTOList = ingredientCategoryService.getAllDTOs();
        imageLink = new TextField("Изображение");
        imageLink.setId("IngredientCategoryImageLinkField");
        ImageUpload imageUpload = new ImageUpload(uploadLocation, pathPattern);
        imageUpload.uploadImage("IngredientCategories", imageLink);
        imageUpload.setAcceptedFileTypes("image/jpeg", "image/png", "image/gif");
        HorizontalLayout choosePic = new HorizontalLayout(imageLink, imageUpload);
        choosePic.setSizeFull();

        buttons = new FormButtonsBar();
        add(name, priority, choosePic, buttons);
    }

    @Override
    public FormButtonsBar getButtons() {
        return buttons;
    }

    @Override
    public void setBinder(Binder<IngredientCategoryDTO> binder, IngredientCategoryDTO entity) {
        this.ingredientCategoryDTO = entity;
        binder.forField(name)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length() > 0, "Поле не может быть пустым")
                .withValidator(field -> field.length() <= 255, "Не более 255 символов")
                .withValidator(field -> {
                    if (name.getValue().equals(ingredientCategoryDTO.getName())) {
                        return true;
                    } else {
                        return ingredientCategoryDTOList.stream()
                                .filter(ingredientCategoryDTO ->
                                        ingredientCategoryDTO.getName().toUpperCase().equals(name.getValue().toUpperCase()))
                                .allMatch(ingredientCategoryDTO -> ingredientCategoryDTO == null);
                    }
                }, "Данное название категории ингредиента уже внесено в базу")
                .bind(IngredientCategoryDTO::getName, IngredientCategoryDTO::setName);
        binder.forField(priority)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> {
                    try {
                        return Integer.parseInt(field) > 0;
                    } catch (Exception e) {
                        return false;
                    }
                }, "Приоритет должен быть целым положительным числом")
                .withValidator(field -> {
                    if (priority.getValue().equals(ingredientCategoryDTO.getPriority())) {
                        return true;
                    } else {
                        List<IngredientCategoryDTO> ingredientCategoryDTOs = ingredientCategoryDTOList.stream()
                                .filter(ingredientCategoryDTO ->
                                        ingredientCategoryDTO.getPriority().equals(priority.getValue()))
                                .collect(Collectors.toList());
                        if (ingredientCategoryDTOs.size() == 0) {
                            return true;
                        } else {
                            Notification.show("Это значение соответствует категории ингредиента : " +
                                    ingredientCategoryDTOs.get(0).getName());
                            return false;
                        }
                    }
                }, "Данный приоритет категории ингредиента уже внесен в базу")
                .bind(IngredientCategoryDTO::getPriority, IngredientCategoryDTO::setPriority);
        binder.forField(imageLink)
                .withValidator(field -> field.length() <= 255, "Не более 255 символов")
                .bind(IngredientCategoryDTO::getImageLink, IngredientCategoryDTO::setImageLink);
    }

    @Override
    public IngredientCategoryDTO getDTO() {
        return ingredientCategoryDTO;
    }

    @Override
    public String getChangedDTOName() {
        return ingredientCategoryDTO.getName();
    }

    @Override
    public Integer getDTOId() {
        return ingredientCategoryDTO.getId();
    }
}
