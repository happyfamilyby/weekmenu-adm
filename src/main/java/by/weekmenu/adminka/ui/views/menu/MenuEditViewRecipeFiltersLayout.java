package by.weekmenu.adminka.ui.views.menu;

import by.weekmenu.adminka.dto.RecipeCategoryDTO;
import by.weekmenu.adminka.dto.RecipeDTO;
import by.weekmenu.adminka.dto.RecipeSubcategoryDTO;
import by.weekmenu.adminka.service.RecipeCategoryService;
import by.weekmenu.adminka.service.RecipeService;
import by.weekmenu.adminka.service.RecipeSubcategoryService;
import by.weekmenu.adminka.ui.util.RecipeFilters;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

public class MenuEditViewRecipeFiltersLayout extends VerticalLayout {

    private final List<RecipeDTO> recipeDTOS;
    private final TextField recipeName;
    private final NumberField totalCookingTime;
    private final NumberField activeCookingTime;
    private final ComboBox<String> recipeCategoryName;
    private final ComboBox<String> recipeSubcategoryName;
    private final NumberField recipeCalories;
    private final NumberField minRecipeCost;
    private final NumberField maxRecipeCost;
    private final Grid<RecipeCard> recipeGrid;
    private RecipeFilters recipeFilters;

    public MenuEditViewRecipeFiltersLayout(RecipeService recipeService, RecipeCategoryService recipeCategoryService,
                                           RecipeSubcategoryService recipeSubcategoryService) {
        recipeFilters = new RecipeFilters();

        recipeDTOS = recipeService.getAllDTOs();
        recipeName = new TextField("Название рецепта");
        recipeName.addValueChangeListener(e -> filterApply());
        recipeName.setValueChangeMode(ValueChangeMode.EAGER);

        totalCookingTime = new NumberField("Общ. время");
        totalCookingTime.setWidth("100px");
        totalCookingTime.addValueChangeListener(e -> filterApply());
        totalCookingTime.setValueChangeMode(ValueChangeMode.EAGER);

        activeCookingTime = new NumberField("Актив. время");
        activeCookingTime.setWidth("100px");
        activeCookingTime.addValueChangeListener(e -> filterApply());
        activeCookingTime.setValueChangeMode(ValueChangeMode.EAGER);

        HorizontalLayout timeFilterHorizontalLayout = new HorizontalLayout();
        timeFilterHorizontalLayout.add(totalCookingTime, activeCookingTime);

        recipeCategoryName = new ComboBox<>("Категория");
        recipeCategoryName.setItems(recipeCategoryService.getAllDTOs().stream()
                .map(RecipeCategoryDTO::getName)
                .collect(Collectors.toList()));
        recipeCategoryName.addValueChangeListener(e -> filterApply());

        recipeSubcategoryName = new ComboBox<>("Подкатегория");
        recipeSubcategoryName.setItems(recipeSubcategoryService.getAllDTOs().stream()
                .map(RecipeSubcategoryDTO::getName)
                .collect(Collectors.toList()));
        recipeSubcategoryName.addValueChangeListener(e -> filterApply());

        recipeCalories = new NumberField("Макс. ккал");
        recipeCalories.setPlaceholder("Макс. ккал");
        recipeCalories.setMin(1d);
        recipeCalories.addValueChangeListener(e -> filterApply());
        recipeCalories.setValueChangeMode(ValueChangeMode.EAGER);

        minRecipeCost = new NumberField("Мин. стоим.");
        minRecipeCost.setPlaceholder("Мин. стоим.");
        minRecipeCost.setWidth("100px");
        minRecipeCost.addValueChangeListener(e -> filterApply());
        minRecipeCost.setValueChangeMode(ValueChangeMode.EAGER);

        maxRecipeCost = new NumberField("Макс. стоим.");
        maxRecipeCost.setPlaceholder("Макс. стоим.");
        maxRecipeCost.setWidth("100px");
        maxRecipeCost.addValueChangeListener(e -> filterApply());
        maxRecipeCost.setValueChangeMode(ValueChangeMode.EAGER);

        HorizontalLayout costFilterHorizontalLayout = new HorizontalLayout(minRecipeCost, maxRecipeCost);
        addClassName("filters-layout");
        setPadding(false);
        VerticalLayout additionalFilterLayout = new VerticalLayout();
        additionalFilterLayout.setVisible(false);
        Button additionalFilters = new Button("Доп. фильтры", click -> {
            if (additionalFilterLayout.isVisible()) {
                additionalFilterLayout.setVisible(false);
            } else {
                additionalFilterLayout.setVisible(true);
            }
        });
        additionalFilterLayout.add(recipeCategoryName, recipeSubcategoryName, recipeCalories,
                timeFilterHorizontalLayout, costFilterHorizontalLayout);
        add(recipeName, additionalFilters, additionalFilterLayout);
        recipeGrid = new Grid<>();
        recipeGrid.setHeightFull();
        recipeGrid.setItems(convertRecipeDTOtoRecipeCard(recipeDTOS));
        recipeGrid.addComponentColumn(recipeCard -> recipeCard).setHeader("Рецепты");
        add(recipeGrid);
    }

    private List<RecipeCard> convertRecipeDTOtoRecipeCard(List<RecipeDTO> list) {
        return list.stream()
                .map(recipeDTO -> {
                    RecipeCard recipeCard = new RecipeCard(recipeDTO.getImageLink(), recipeDTO.getName(), null, null, null);
                    recipeCard.getIconRow().setVisible(false);
                    return recipeCard;
                })
                .collect(Collectors.toList());
    }

    private void filterApply() {
        List<RecipeDTO> filteredRecipeDTOS = recipeDTOS;
        if (!StringUtils.isEmpty(recipeName.getValue())) {
            filteredRecipeDTOS = recipeFilters
                    .filterRecipeDTObyName(filteredRecipeDTOS, recipeName.getValue());
        }
        if (totalCookingTime.getValue() != null) {
            filteredRecipeDTOS = recipeFilters
                    .filterRecipeDTObyTime(filteredRecipeDTOS, totalCookingTime.getValue().shortValue()
                            , "Filter by total time");
        }
        if (activeCookingTime.getValue() != null) {
            filteredRecipeDTOS = recipeFilters
                    .filterRecipeDTObyTime(filteredRecipeDTOS, activeCookingTime.getValue().shortValue()
                            , "Filter by active time");
        }
        if (recipeCalories.getValue() != null) {
            filteredRecipeDTOS = recipeFilters
                    .filterRecipeDTObyRecipeCalories(filteredRecipeDTOS, recipeCalories.getValue().intValue());
        }
        if (!StringUtils.isEmpty(recipeCategoryName.getValue())) {
            filteredRecipeDTOS = recipeFilters
                    .filterRecipeDTObyCategories(filteredRecipeDTOS, recipeCategoryName.getValue()
                            , "Filter by recipe category name");
        }
        if (!StringUtils.isEmpty(recipeSubcategoryName.getValue())) {
            filteredRecipeDTOS = recipeFilters
                    .filterRecipeDTObyCategories(filteredRecipeDTOS, recipeSubcategoryName.getValue()
                            , "Filter by recipe subcategory name");
        }
        if (minRecipeCost.getValue() != null || maxRecipeCost.getValue() != null) {
            filteredRecipeDTOS = recipeFilters
                    .filterRecipeDTObyCost(filteredRecipeDTOS, minRecipeCost, maxRecipeCost);
        }
        recipeGrid.setItems(convertRecipeDTOtoRecipeCard(filteredRecipeDTOS));
    }
}
