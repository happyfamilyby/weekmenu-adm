package by.weekmenu.adminka.ui.views.ingredient;

import by.weekmenu.adminka.dto.IngredientCategoryDTO;
import by.weekmenu.adminka.service.IngredientCategoryService;
import by.weekmenu.adminka.ui.MainView;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.views.CrudForm;
import by.weekmenu.adminka.ui.views.CrudView;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = AppConsts.PAGE_INGREDIENT_CATEGORY, layout = MainView.class)
@PageTitle(AppConsts.TITLE_INGREDIENT_CATEGORY)
public class IngredientCategoryView extends CrudView<IngredientCategoryDTO, Integer> {

    private final Binder<IngredientCategoryDTO> binder;

    @Autowired
    public IngredientCategoryView(CrudForm<IngredientCategoryDTO, Integer> form, IngredientCategoryService serviceClient) {
        super(form, serviceClient);
        binder = new Binder<>(IngredientCategoryDTO.class);
        if (serviceClient.getAllDTOs() != null) {
            getGrid().setItems(serviceClient.getAllDTOs());
        }
        getSearchBar().getSearchField().setVisible(false);
    }

    @Override
    public Binder<IngredientCategoryDTO> getBinder() {
        return binder;
    }

    @Override
    protected void setupGrid() {
        getGrid().addColumn(new ComponentRenderer<>(IngredientCategoryDTO -> {
            if ((IngredientCategoryDTO.getImageLink() != null) && !StringUtils.isEmpty(IngredientCategoryDTO.getImageLink())) {
                Image image = new Image(IngredientCategoryDTO.getImageLink(), IngredientCategoryDTO.getName());
                image.addClassName("small-image");
                return image;
            } else {
                Image imageEmpty = new Image("", "No image");
                imageEmpty.addClassName("small-image");
                return imageEmpty;
            }
        }
        ))
                .setHeader("Фото");
        getGrid().addColumn(IngredientCategoryDTO::getName).setHeader("Категория ингредиента");
        getGrid().addColumn(IngredientCategoryDTO::getPriority).setHeader("Приоритет категории ингредиента");
    }
}
