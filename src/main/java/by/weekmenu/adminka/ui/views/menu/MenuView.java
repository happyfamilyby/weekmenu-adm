package by.weekmenu.adminka.ui.views.menu;

import by.weekmenu.adminka.dto.MenuDTO;
import by.weekmenu.adminka.dto.MenuPriceDTO;
import by.weekmenu.adminka.service.MenuService;
import by.weekmenu.adminka.ui.MainView;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.util.Converters;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Route(value = AppConsts.PAGE_MENU, layout = MainView.class)
@PageTitle(AppConsts.TITLE_MENU)
public class MenuView extends VerticalLayout implements HasUrlParameter<String> {

    @Value("${token}")
    private String token;

    private final Grid<MenuDTO> grid;
    private final H1 h1;
    private final H1 matchesNotFound;
    private final TextField searchField;
    private final ComboBox<String> filterMenuByActiveField;
    private final List<MenuDTO> menuDTOList;
    private final TextField weekNumberSearchField;

    @Autowired
    public MenuView(MenuService menuService) {
        addClassName("menu-view");
        setSizeFull();
        setDefaultHorizontalComponentAlignment(FlexComponent.Alignment.CENTER);
        menuDTOList = menuService.getAllDTOs();
        searchField = new TextField("Поиск по названию");
        searchField.setId("SearchField");
        searchField.setValueChangeMode(ValueChangeMode.EAGER);
        searchField.setClearButtonVisible(true);
        searchField.setWidth("475px");
        filterMenuByActiveField = new ComboBox();
        filterMenuByActiveField.setId("FilterMenuByActiveField");
        filterMenuByActiveField.setItems("Все", "Активные", "Неактивные");
        filterMenuByActiveField.setLabel("Фильтр меню по активности");
        filterMenuByActiveField.setWidth("250px");
        filterMenuByActiveField.setValue("Все");
        weekNumberSearchField = new TextField("№ недели");
        weekNumberSearchField.setId("WeekNumberMenuSearchField");
        weekNumberSearchField.setValueChangeMode(ValueChangeMode.EAGER);
        weekNumberSearchField.setClearButtonVisible(true);
        weekNumberSearchField.setWidth("80px");
        Button addMenu = new Button("Добавить", e -> UI.getCurrent().navigate(AppConsts.PAGE_MENU_EDIT + "/" + token));
        addMenu.setId("AddMenuButton");
        addMenu.addThemeVariants(ButtonVariant.MATERIAL_CONTAINED);
        HorizontalLayout searchBar = new HorizontalLayout(weekNumberSearchField, searchField, filterMenuByActiveField, addMenu);
        searchBar.addClassName("search-bar");
        add(searchBar);
        grid = new Grid<>();
        h1 = new H1("Пока ничего не добавлено");
        matchesNotFound = new H1("Совпадений не найдено");
        if (menuDTOList.size() > 0) {
            updateList(null, menuDTOList, "DefaultView");
        } else {
            add(h1);
            searchField.setVisible(false);
            filterMenuByActiveField.setVisible(false);
            weekNumberSearchField.setVisible(false);
        }
        setupGrid();
        searchField.addValueChangeListener(event -> {
            if (!StringUtils.isEmpty(event.getValue())) {
                updateList(event.getValue(), menuDTOList, "SearchMenuByName");
            } else updateList(null, menuDTOList, "DefaultView");
        });
        filterMenuByActiveField.addValueChangeListener(event -> {
            if (!StringUtils.isEmpty(event.getValue())) {
                updateList(event.getValue(), menuDTOList, "SearchMenuByActivity");
            } else updateList(null, menuDTOList, "DefaultView");
        });
        weekNumberSearchField.addValueChangeListener(event -> {
            String regex = "^[0-9]{1,2}$";
            Pattern pattern = Pattern.compile(regex);
            if (!StringUtils.isEmpty(event.getValue())) {
                Matcher matcher = pattern.matcher(event.getValue());
                if (matcher.find()) {
                    updateList(event.getValue(), menuDTOList, "SearchMenuByWeekNumber");
                } else Notification.show("Введите 1-2 цифры");
            } else updateList(null, menuDTOList, "DefaultView");
        });
        grid.asSingleSelect().addValueChangeListener(e -> UI.getCurrent()
                .navigate(AppConsts.PAGE_MENU_EDIT + "/" + token + "/" + e.getValue().getId()));
    }

    private void updateList(String search, List<MenuDTO> menuDTOList, String menuFilterField) {
        switch (menuFilterField) {
            case "DefaultView": {
                refreshMenuView(menuDTOList);
                break;
            }
            case "SearchMenuByName": {
                refreshMenuView(menuDTOList.stream().filter(menuDTO -> menuDTO.getName().toLowerCase()
                        .contains(search.toLowerCase()))
                        .collect(Collectors.toList()));
                break;
            }
            case "SearchMenuByWeekNumber": {
                refreshMenuView(menuDTOList.stream().filter(menuDTO -> menuDTO.getWeekNumber()
                        .equals(search)).collect(Collectors.toList()));
                break;
            }
            case "SearchMenuByActivity": {
                if (search.equals("Активные")) {
                    refreshMenuView(menuDTOList.stream().filter(menuDTO -> menuDTO.getIsActive()
                            .equals(true)).collect(Collectors.toList()));
                } else if (search.equals("Неактивные")) {
                    refreshMenuView(menuDTOList.stream().filter(menuDTO -> menuDTO.getIsActive()
                            .equals(false)).collect(Collectors.toList()));
                } else refreshMenuView(menuDTOList);
                break;
            }
        }
    }

    private void refreshMenuView(List<MenuDTO> filteredMenuDTOList) {
        if (filteredMenuDTOList.size() > 0) {
            remove(matchesNotFound);
            remove(h1);
            grid.setItems(filteredMenuDTOList);
            add(grid);
            searchField.setVisible(true);
            filterMenuByActiveField.setVisible(true);
            weekNumberSearchField.setVisible(true);
        } else {
            remove(grid);
            remove(h1);
            add(matchesNotFound);
        }
    }

    private void setupGrid() {
        grid.addColumn(MenuDTO::getWeekNumber).setHeader("N нед.").setWidth("10px").setResizable(true);
        grid.addColumn(MenuDTO::getName).setHeader("Меню").setWidth("400px").setResizable(true);
        grid.addColumn(MenuDTO::getMenuCategoryName).setHeader("Категория меню").setResizable(true);
        grid.addColumn(new ComponentRenderer<>(menuDTO -> {
            Checkbox checkbox = new Checkbox();
            checkbox.setValue(menuDTO.getIsActive());
            checkbox.setEnabled(false);
            return checkbox;
        })).setHeader("Активность").setWidth("20px").setResizable(true);
        grid.addColumn(menuDTO ->
                String.join(" / ",
                        Converters.convertBigDecimalToString(menuDTO.getCalories()),
                        Converters.convertBigDecimalToString(menuDTO.getProteins()),
                        Converters.convertBigDecimalToString(menuDTO.getFats()),
                        Converters.convertBigDecimalToString(menuDTO.getCarbs())
                )).setHeader("Среднедневное К/Б/Ж/У на 1 чел.").setWidth("40px").setResizable(true);
        grid.addColumn(new ComponentRenderer<>(this::showRegionPrices)).setHeader("Стоимость 1 меню")
                .setWidth("30px").setResizable(true);
    }

    private Component showRegionPrices(MenuDTO menuDTO) {
        VerticalLayout layout = new VerticalLayout();
        for (MenuPriceDTO menuPriceDTO : menuDTO.getMenuPriceDTOS()) {
            layout.add(new HorizontalLayout(new Label(menuPriceDTO.getRegionName()),
                    new Label(menuPriceDTO.getPriceValue().replace('.', ',')), new Label(menuPriceDTO.getCurrencyCode())));
        }
        return layout;
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, String s) {
        if (!s.equals(token)) {
            throw new IllegalArgumentException();
        }
    }
}
