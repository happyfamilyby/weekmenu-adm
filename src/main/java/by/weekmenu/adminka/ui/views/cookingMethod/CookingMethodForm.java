package by.weekmenu.adminka.ui.views.cookingMethod;

import by.weekmenu.adminka.dto.CookingMethodDTO;
import by.weekmenu.adminka.service.CookingMethodService;
import by.weekmenu.adminka.ui.components.FormButtonsBar;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import java.util.List;
import java.util.stream.Collectors;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CookingMethodForm extends VerticalLayout implements CrudForm<CookingMethodDTO, Integer> {

    private final TextField name;
    private final TextField priority;
    private final FormButtonsBar buttons;
    private CookingMethodDTO cookingMethodDTO;
    private List<CookingMethodDTO> cookingMethodDTOList;

    public CookingMethodForm(CookingMethodService cookingMethodService) {
        setSizeFull();
        setId("CookingMethodForm");
        name = new TextField("Способ приготовления");
        name.setAutofocus(true);
        name.setId("CookingMethodNameField");

        cookingMethodDTOList = cookingMethodService.getAllDTOs();

        priority = new TextField("Приоритет способа приготовления");
        priority.setId("CookingMethodPriorityField");

        buttons = new FormButtonsBar();
        add(name, priority, buttons);
    }

    @Override
    public FormButtonsBar getButtons() {
        return buttons;
    }

    @Override
    public void setBinder(Binder<CookingMethodDTO> binder, CookingMethodDTO entity) {
        this.cookingMethodDTO = entity;
        binder.forField(name)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length() > 0, "Поле не может быть пустым")
                .withValidator(field -> field.length() <= 255, "Не более 255 символов")
                .withValidator(field -> {
                    if (name.getValue().equals(cookingMethodDTO.getName())) {
                        return true;
                    } else {
                        return cookingMethodDTOList.stream()
                                .filter(cookingMethodDTO ->
                                        cookingMethodDTO.getName().toUpperCase().equals(name.getValue().toUpperCase()))
                                .allMatch(cookingMethodDTO -> cookingMethodDTO == null);
                    }
                }, "Данное название способа приготовления рецепта уже внесено в базу")
                .bind(CookingMethodDTO::getName, CookingMethodDTO::setName);
        binder.forField(priority)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> {
                    try {
                        return Integer.parseInt(field) > 0;
                    } catch (Exception e) {
                        return false;
                    }
                }, "Приоритет должен быть целым положительным числом")
                .withValidator(field -> {
                    if (priority.getValue().equals(cookingMethodDTO.getPriority())) {
                        return true;
                    } else {
                        List<CookingMethodDTO> cookingMethodDTOs = cookingMethodDTOList.stream()
                                .filter(cookingMethodDTO ->
                                        (!(cookingMethodDTO.getPriority() == null)))
                                .filter(cookingMethodDTO ->
                                        cookingMethodDTO.getPriority().equals(priority.getValue()))
                                .collect(Collectors.toList());
                        if (cookingMethodDTOs.size() == 0) {
                            return true;
                        } else {
                            Notification.show("Это значение соответствует способу приготовления рецепта: " +
                                    cookingMethodDTOs.get(0).getName());
                            return false;
                        }
                    }
                }, "Данный приоритет способа приготовления уже внесен в базу")
                .bind(CookingMethodDTO::getPriority, CookingMethodDTO::setPriority);
    }

    @Override
    public CookingMethodDTO getDTO() {
        return cookingMethodDTO;
    }

    @Override
    public String getChangedDTOName() {
        return cookingMethodDTO.getName();
    }

    @Override
    public Integer getDTOId() {
        return cookingMethodDTO.getId();
    }
}
