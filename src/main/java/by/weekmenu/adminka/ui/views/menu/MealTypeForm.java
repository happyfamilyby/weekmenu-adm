package by.weekmenu.adminka.ui.views.menu;

import by.weekmenu.adminka.dto.MealTypeDTO;
import by.weekmenu.adminka.service.MealTypeService;
import by.weekmenu.adminka.ui.components.FormButtonsBar;
import by.weekmenu.adminka.ui.views.CrudForm;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MealTypeForm extends VerticalLayout implements CrudForm<MealTypeDTO, Short> {

    private final TextField name;
    private final TextField priority;
    private final FormButtonsBar buttons;
    private MealTypeDTO mealTypeDTO;
    private final MealTypeService mealTypeService;

    public MealTypeForm(MealTypeService mealTypeService) {
        this.mealTypeService = mealTypeService;
        setSizeFull();
        setId("MealTypeForm");
        name = new TextField("Приём пищи");
        name.setAutofocus(true);
        name.setId("MealTypeNameField");

        priority = new TextField("Приоритет приёма пищи");
        priority.setId("MealTypePriorityField");

        buttons = new FormButtonsBar();
        add(name, priority, buttons);
    }

    @Override
    public FormButtonsBar getButtons() {
        return buttons;
    }

    @Override
    public void setBinder(Binder<MealTypeDTO> binder, MealTypeDTO entity) {
        this.mealTypeDTO = entity;
        binder.forField(name)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> field.trim().length() > 0, "Поле не может быть пустым")
                .withValidator(field -> field.length() <= 255, "Не более 255 символов")
                .withValidator(field -> {
                    if (name.getValue().equals(mealTypeDTO.getName())) {
                        return true;
                    } else {
                        return mealTypeService.checkMealTypeUniqueName(field) == 0;
                    }
                }, "Данное название приёма пищи уже внесено в базу")
                .bind(MealTypeDTO::getName, MealTypeDTO::setName);
        binder.forField(priority)
                .asRequired("Поле не может быть пустым")
                .withValidator(field -> {
                    if (!StringUtils.isEmpty(field)) {
                        try {
                            return Integer.parseInt(field) > 0;
                        } catch (Exception e) {
                            return false;
                        }
                    } else {
                        return true;
                    }
                }, "Приоритет должен быть положительным")
                .withValidator(field -> {
                    if (priority.getValue().equals(mealTypeDTO.getPriority())) {
                        return true;
                    } else {
                        if (!(mealTypeService.checkMealTypeUniquePriority(field) == 0)) {
                            for (MealTypeDTO mealTypeDTO : mealTypeService.getAllDTOs()) {
                                if (mealTypeDTO.getPriority().equals(field)) {
                                    Notification.show("Это значение соответствует приёму пищи : " + mealTypeDTO.getName());
                                }
                            }
                        }
                        return mealTypeService.checkMealTypeUniquePriority(field) == 0;
                    }
                }, "Данный приоритет приёма пищи уже внесен в базу")
                .bind(MealTypeDTO::getPriority, MealTypeDTO::setPriority);
    }

    @Override
    public MealTypeDTO getDTO() {
        return mealTypeDTO;
    }

    @Override
    public String getChangedDTOName() {
        return mealTypeDTO.getName();
    }

    @Override
    public Short getDTOId() {
        return mealTypeDTO.getId();
    }
}
