package by.weekmenu.adminka.ui.views.menu;

import by.weekmenu.adminka.dto.MenuRecipeDTO;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import org.vaadin.stefan.dnd.drop.DropTargetExtension;

import java.time.DayOfWeek;
import java.util.*;
import java.util.stream.Collectors;

class MenuGridRow extends HorizontalLayout {

    private final List<MenuRecipeDTO> menuRecipeDTOS = new ArrayList<>();

    private final List<VerticalLayout> cells;
    private Button mealTypeNameButton;
    private Integer priority;

    public MenuGridRow() {
        setSizeFull();
        VerticalLayout cellMon = new VerticalLayout();
        VerticalLayout cellTue = new VerticalLayout();
        VerticalLayout cellWed = new VerticalLayout();
        VerticalLayout cellThu = new VerticalLayout();
        VerticalLayout cellFri = new VerticalLayout();
        VerticalLayout cellSat = new VerticalLayout();
        VerticalLayout cellSun = new VerticalLayout();
        cells = Arrays.asList(cellMon, cellTue, cellWed, cellThu, cellFri, cellSat, cellSun);
    }

    public void createRow(String mealName) {
        mealTypeNameButton = new Button(mealName, click -> {
            if (this.isVisible()) {
                this.setVisible(false);
            } else {
                this.setVisible(true);
            }
        });
        mealTypeNameButton.setWidthFull();
        for (int i = 0; i < 7; i++) {
            VerticalLayout verticalLayout = cells.get(i);
            DropTargetExtension<VerticalLayout> dropTargetExtension = DropTargetExtension.extend(verticalLayout);
            DayOfWeek dayOfWeek = DayOfWeek.of(i + 1);
            dropTargetExtension.addDropListener(event -> event.getDragSource().ifPresent(e -> {
                RecipeCard component = (RecipeCard) e.getComponent();
                boolean componentIsFromGrid = false;
                if (component.getDayOfWeek() == null) componentIsFromGrid = true;
                List rec = event.getComponent().getChildren().collect(Collectors.toList());
                List<RecipeCard> recipeCardList = new ArrayList<>();
                for (Object recipeCard : rec) {
                    recipeCardList.add((RecipeCard) recipeCard);
                }
                if ((recipeCardList.size() == 0) || (!(recipeCardList.stream().anyMatch(recipeCard -> recipeCard.equals(component))))) {
                    if (!componentIsFromGrid) {
                        event.getComponent().add(component);
                        deleteMenuRecipeDTO(component);
                        component.setDayOfWeek(dayOfWeek);
                        addMenuRecipeDTO(mealName, dayOfWeek, component);
                    } else {
                        RecipeCard card = new RecipeCard(component.getImageLink(), component.getRecipeName(), mealName, dayOfWeek, this);
                        card.setPriority(recipeCardList.size() + 1);
                        event.getComponent().add(card);
                        addMenuRecipeDTO(mealName, dayOfWeek, card);
                    }
                } else {
                    if (recipeCardList.stream()
                            .anyMatch(recipecard -> recipecard.equals(component)
                                    && ((recipecard.getDayOfWeek() != component.getDayOfWeek()))
                                    && component.getDayOfWeek() != null)) {
                        Notification.show("Этот рецепт уже присутствует в списке.");
                    } else if (recipeCardList.stream()
                            .anyMatch(recipecard -> recipecard.equals(component)
                                    && ((recipecard.getDayOfWeek() != component.getDayOfWeek()))
                                    && component.getDayOfWeek() == null)) {
                        recipeCardList.removeIf((RecipeCard recipeCard) -> recipeCard.equals(component));
                        RecipeCard card = new RecipeCard(component.getImageLink(), component.getRecipeName(), mealName, dayOfWeek, this);
                        card.setPriority(recipeCardList.size() + 1);
                        event.getComponent().removeAll();
                        recipeCardList.stream().forEach(recipeCard -> event.getComponent().add(recipeCard));
                        event.getComponent().add(card);
                        component.setPriority(recipeCardList.size() + 1);
                        addMenuRecipeDTO(mealName, dayOfWeek, component);
                    } else {
                        if (recipeCardList.size() == 1 && recipeCardList.get(0).equals(component)) {
                            Notification.show("В списке 1 рецепт. Его не с чем менять местами.");
                        } else {
                            if (component.getPriority() == recipeCardList.size()) {
                                for (RecipeCard recipeCard : recipeCardList) {
                                    if (recipeCard.getPriority() == recipeCardList.size()) {
                                        deleteMenuRecipeDTO(recipeCard);
                                        recipeCard.setPriority(1);
                                        addMenuRecipeDTO(mealName, dayOfWeek, recipeCard);
                                    }
                                    if (recipeCard.getPriority() == 1) {
                                        deleteMenuRecipeDTO(recipeCard);
                                        recipeCard.setPriority(component.getPriority());
                                        addMenuRecipeDTO(mealName, dayOfWeek, recipeCard);
                                    }
                                }
                                sortRecipeCardsInVerticalLayout(event.getComponent(), recipeCardList);
                                Notification.show("Рецепт " + component.getRecipeName() + " перемещен на позицию 1.");
                            } else {
                                RecipeCard tempRecipeCard1 = null, tempRecipeCard2 = null;
                                for (RecipeCard recipeCard : recipeCardList) {
                                    if (recipeCard.getPriority() == component.getPriority() + 1) {
                                        deleteMenuRecipeDTO(recipeCard);
                                        tempRecipeCard1 = recipeCard;
                                    }
                                    if (recipeCard.getPriority() == component.getPriority()) {
                                        deleteMenuRecipeDTO(recipeCard);
                                        tempRecipeCard2 = recipeCard;
                                    }
                                }
                                tempRecipeCard1.setPriority(component.getPriority());
                                tempRecipeCard2.setPriority(component.getPriority() + 1);
                                addMenuRecipeDTO(mealName, dayOfWeek, tempRecipeCard1);
                                addMenuRecipeDTO(mealName, dayOfWeek, tempRecipeCard2);
                                sortRecipeCardsInVerticalLayout(event.getComponent(), recipeCardList);
                                Notification.show("Рецепт " + component.getRecipeName() + " перемещен на 1 позицию вниз.");
                            }
                        }
                    }
                }
            }));
            verticalLayout.getStyle().set("border", "dotted");
            add(verticalLayout);
        }
    }

    public void createCard(Set<MenuRecipeDTO> menuRecipeDTOS) {
        for (int i = 1; i < 8; i++) {
            VerticalLayout verticalLayout = cells.get(i - 1);
            int finalI = i;
            List<RecipeCard> recipeCardsFoVerticalLayouts = new ArrayList<>();
            menuRecipeDTOS.stream().filter(menuRecipeDTO -> menuRecipeDTO.getDayOfWeek().getValue() == finalI)
                    .forEach(menuRecipeDTO -> {
                        if (mealTypeNameButton.getText().equals(menuRecipeDTO.getMealTypeName())) {
                            RecipeCard recipeCard = new RecipeCard(menuRecipeDTO.getRecipeImageLink(),
                                    menuRecipeDTO.getRecipeName(),
                                    menuRecipeDTO.getMealTypeName(),
                                    menuRecipeDTO.getDayOfWeek(), this);
                            recipeCard.setPriority(Integer.valueOf(menuRecipeDTO.getPriority()));
                            recipeCardsFoVerticalLayouts.add(recipeCard);
                            addMenuRecipeDTO(menuRecipeDTO.getMealTypeName(), menuRecipeDTO.getDayOfWeek(), recipeCard);
                        }
                    });
            sortRecipeCardsInVerticalLayout(verticalLayout, recipeCardsFoVerticalLayouts);
        }
    }

    private void addMenuRecipeDTO(String mealName, DayOfWeek dayOfWeek, RecipeCard card) {
        MenuRecipeDTO menuRecipeDTO = new MenuRecipeDTO();
        menuRecipeDTO.setDayOfWeek(dayOfWeek);
        menuRecipeDTO.setMealTypeName(mealName);
        menuRecipeDTO.setRecipeName(card.getRecipeName());
        menuRecipeDTO.setRecipeImageLink(card.getImageLink());
        menuRecipeDTO.setPriority(card.getPriority().toString());
        menuRecipeDTOS.add(menuRecipeDTO);
    }

    public void deleteMenuRecipeDTO(RecipeCard recipeCard) {
        MenuRecipeDTO menuRecipeDTO = new MenuRecipeDTO();
        menuRecipeDTO.setRecipeImageLink(recipeCard.getImageLink());
        menuRecipeDTO.setDayOfWeek(recipeCard.getDayOfWeek());
        menuRecipeDTO.setMealTypeName(recipeCard.getMealTypeName());
        menuRecipeDTO.setRecipeName(recipeCard.getRecipeName());
        menuRecipeDTO.setPriority(recipeCard.getPriority().toString());
        menuRecipeDTOS.remove(menuRecipeDTO);
    }

    public List<MenuRecipeDTO> getMenuRecipeDTOS() {
        return menuRecipeDTOS;
    }

    public Button getMealTypeNameButton() {
        return mealTypeNameButton;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public VerticalLayout sortRecipeCardsInVerticalLayout(VerticalLayout layout, List<RecipeCard> recipeCardList) {
        layout.removeAll();
        recipeCardList.sort(new Comparator<RecipeCard>() {
            @Override
            public int compare(RecipeCard recipeCard1, RecipeCard recipeCard2) {
                return recipeCard1.getPriority() - recipeCard2.getPriority();
            }
        });
        recipeCardList.stream().forEach(recipeCard -> layout.add(recipeCard));
        return layout;
    }
}
