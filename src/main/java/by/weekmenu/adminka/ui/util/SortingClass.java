package by.weekmenu.adminka.ui.util;

import by.weekmenu.adminka.dto.CategorySuperDTO;
import by.weekmenu.adminka.dto.IngredientDTO;
import by.weekmenu.adminka.dto.MenuDTO;

import java.util.Comparator;
import java.util.List;

public class SortingClass {

    public static List<CategorySuperDTO> sortByPriority(List entityDTOs) {
        entityDTOs.sort(new Comparator<CategorySuperDTO>() {
            @Override
            public int compare(CategorySuperDTO cat1, CategorySuperDTO cat2) {
                return Integer.parseInt(cat1.getPriority()) - Integer.parseInt(cat2.getPriority());
            }
        });
        return entityDTOs;
    }

    public static List<IngredientDTO> sortIngredientsByName(List ingredients) {
        ingredients.sort(new Comparator<IngredientDTO>() {
            @Override
            public int compare(IngredientDTO ing1, IngredientDTO ing2) {
                return ing1.getName().compareTo(ing2.getName());

            }
        });
        return ingredients;
    }

    public static List<MenuDTO> sortByWeeknumber(List entityDTOs) {
        entityDTOs.sort(new Comparator<MenuDTO>() {
            @Override
            public int compare(MenuDTO menu1, MenuDTO menu2) {
                return Integer.parseInt(menu1.getWeekNumber()) - Integer.parseInt(menu2.getWeekNumber());
            }
        });
        return entityDTOs;
    }
}
