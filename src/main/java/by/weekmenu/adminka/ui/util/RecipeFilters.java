package by.weekmenu.adminka.ui.util;

import by.weekmenu.adminka.dto.RecipeDTO;
import by.weekmenu.adminka.dto.RecipePriceDTO;
import com.vaadin.flow.component.textfield.NumberField;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RecipeFilters {

    public List<RecipeDTO> filterRecipeDTObyName(List<RecipeDTO> recipeDTOS, String recipeName) {
        return recipeDTOS.stream().filter(recipeDTO ->
                recipeDTO.getName().toLowerCase().contains(recipeName.toLowerCase()))
                .collect(Collectors.toList());
    }

    public List<RecipeDTO> filterRecipeDTObyTime(List<RecipeDTO> recipeDTOS, Short time, String choiceTimeFilter) {
        switch (choiceTimeFilter) {
            case "Filter by total time": {
                return recipeDTOS.stream().filter(recipeDTO ->
                        Short.parseShort(recipeDTO.getCookingTime()) <= time)
                        .collect(Collectors.toList());
            }
            case "Filter by active time": {
                return recipeDTOS.stream().filter(recipeDTO ->
                        Short.parseShort(recipeDTO.getActiveTime()) <= time)
                        .collect(Collectors.toList());
            }
            default:
                return null;
        }
    }

    public List<RecipeDTO> filterRecipeDTObyRecipeCalories(List<RecipeDTO> recipeDTOS, Integer recipeCalories) {
        return recipeDTOS.stream().filter(recipeDTO ->
                Integer.parseInt(recipeDTO.getCalories().setScale(0, BigDecimal.ROUND_HALF_UP).toString()) <=
                        recipeCalories)
                .collect(Collectors.toList());
    }

    public List<RecipeDTO> filterRecipeDTObyCategories(List<RecipeDTO> recipeDTOS, String category, String categoryChoice) {
        switch (categoryChoice) {
            case "Filter by recipe category name": {
                return recipeDTOS.stream().filter(recipeDTO ->
                        recipeDTO.getCategoryNames().contains(category))
                        .collect(Collectors.toList());
            }
            case "Filter by recipe subcategory name": {
                return recipeDTOS.stream().filter(recipeDTO ->
                        recipeDTO.getSubcategoryNames().contains(category))
                        .collect(Collectors.toList());
            }
            default:
                return null;
        }
    }

    public List<RecipeDTO> filterRecipeDTObyCost(List<RecipeDTO> recipeDTOS, NumberField minRecipeCost,
                                                 NumberField maxRecipeCost) {
        List<RecipePriceDTO> recipePriceDTOList = new ArrayList<>();
        recipeDTOS.stream().forEach(recipeDTO -> recipeDTO.getRecipePrices().stream()
                .forEach(recipePriceDTO -> recipePriceDTOList.add(recipePriceDTO)));
        List<RecipePriceDTO> filteredRecipePriceDTOSet = recipePriceDTOList.stream()
                .filter(recipePriceDTO -> recipePriceDTO.getRegionName().equals("Минск"))
                .collect(Collectors.toList());
        if (minRecipeCost != null) {
            filteredRecipePriceDTOSet = filteredRecipePriceDTOSet.stream()
                    .filter(recipePriceDTO -> Double.parseDouble(recipePriceDTO.getPriceValue()) >= minRecipeCost.getValue())
                    .collect(Collectors.toList());
        }
        if (maxRecipeCost.getValue() != null) {
            filteredRecipePriceDTOSet = filteredRecipePriceDTOSet.stream()
                    .filter(recipePriceDTO -> Double.parseDouble(recipePriceDTO.getPriceValue()) <= maxRecipeCost.getValue())
                    .collect(Collectors.toList());
        }
        List<String> recipeNames = new ArrayList<>();
        filteredRecipePriceDTOSet.stream().forEach(recipePriceDTO -> recipeNames.add(recipePriceDTO.getRecipeName()));
        return recipeDTOS.stream().filter(recipeDTO ->
                recipeNames.contains(recipeDTO.getName())).collect(Collectors.toList());
    }
}
