package by.weekmenu.adminka.ui.components;

import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.*;

import java.util.ArrayList;
import java.util.List;

public class VerticalMenu extends VerticalLayout implements AfterNavigationObserver {

    private final List<RouterLink> links = new ArrayList<>();

    public VerticalMenu() {
        addClassName("vertical-menu");
        setSizeUndefined();
    }

    public void initMenu(List<PageInfo> items, String token) {
        for (PageInfo pageInfo : items) {
            RouterLink link = new RouterLink(pageInfo.getTitle(), pageInfo.getClazz(), token);
            links.add(link);
            add(new HorizontalLayout(new Icon("vaadin", pageInfo.getIcon()), link));
        }
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {
        String path = event.getLocation().getPath();
        links.stream().filter(routerLink -> !routerLink.getHref().equals(path))
                .forEach(routerLink -> routerLink.getStyle().set("color", "grey").set("text-decoration", "none"));
    }
}
