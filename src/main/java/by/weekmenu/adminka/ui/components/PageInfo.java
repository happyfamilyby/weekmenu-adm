package by.weekmenu.adminka.ui.components;

public class PageInfo {

    private final String link;
    private final String icon;
    private final String title;
    private Class clazz;

    public PageInfo(String link, String icon, String title) {
        this.link = link;
        this.icon = icon;
        this.title = title;
    }

    public PageInfo(String link, String icon, String title, Class clazz) {
        this.link = link;
        this.icon = icon;
        this.title = title;
        this.clazz = clazz;
    }

    public String getLink() {
        return link;
    }

    public String getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }

    public Class getClazz() {return clazz;}
}