package by.weekmenu.adminka.ui.components;


import by.weekmenu.adminka.ui.util.AppConsts;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;

import java.util.ArrayList;
import java.util.List;

public class AppNavigation extends HorizontalLayout implements AfterNavigationObserver {

    private String token;

    private final Tabs tabs = new Tabs();

    private final List<String> hrefs = new ArrayList<>();
    private String currentHref;
    private String defaultHref;

    public void init(List<PageInfo> pages, String defaultHref, String token) {
        addClassName("app-navigation");
        this.defaultHref = defaultHref;
        this.token = token;

        for (PageInfo page : pages) {
            Tab tab = new Tab(new Icon("vaadin", page.getIcon()), new Span(page.getTitle()));
            tab.setId(page.getLink() + "Tab");
            hrefs.add(page.getLink());
            tabs.add(tab);
        }
        add(tabs);
        tabs.addSelectedChangeListener(e -> navigate());
    }

    private void navigate() {
        int tabId = tabs.getSelectedIndex();
        if (tabId >= 0 && tabId < hrefs.size()) {
            String href = hrefs.get(tabId);
            if (!href.equals(currentHref)) {
                UI.getCurrent().navigate(href + "/" + token);
            }
        }
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {
        String href = event.getLocation().getFirstSegment().isEmpty() ? defaultHref
                : event.getLocation().getPath();
        currentHref = href;
        String clearHref = href.split("/")[0];
        switch (clearHref) {
            case AppConsts.PAGE_RECIPE:
            case AppConsts.PAGE_COOKINGMETHODS:
            case AppConsts.PAGE_RECIPECATEGORY:
            case AppConsts.PAGE_RECIPESUBCATEGORY:
                tabs.setSelectedIndex(hrefs.indexOf(AppConsts.PAGE_RECIPE));
                break;
            case AppConsts.PAGE_MENU:
            case AppConsts.PAGE_MENUCATEGORY:
            case AppConsts.PAGE_MEAL_TYPE:
                tabs.setSelectedIndex(hrefs.indexOf(AppConsts.PAGE_MENU));
                break;
            case AppConsts.PAGE_RECYCLE_BIN:
                tabs.setSelectedIndex(hrefs.indexOf(AppConsts.PAGE_RECYCLE_BIN));
                break;
            default:
                tabs.setSelectedIndex(hrefs.indexOf(AppConsts.PAGE_INGREDIENT));
                break;
        }
    }
}
