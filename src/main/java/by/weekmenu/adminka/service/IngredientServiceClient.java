package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.IngredientDTO;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.util.SortingClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class IngredientServiceClient implements IngredientService {

    private final String urlIngredients;
    private final RestTemplate restTemplate;
    private List<IngredientDTO> ingredients = new ArrayList<>();


    @Autowired
    public IngredientServiceClient(RestTemplate restTemplate,
                                   @Value("${weekmenu.server.url}") String baseUrl) {
        this.restTemplate = restTemplate;
        urlIngredients = baseUrl + AppConsts.PAGE_INGREDIENT;
    }

    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Override
    public List<IngredientDTO> getAllDTOs() {
        ingredients = restTemplate.exchange(urlIngredients, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<IngredientDTO>>() {}).getBody();
        SortingClass.sortIngredientsByName(ingredients);
        return ingredients;
    }

    @Override
    public IngredientDTO createNewDTO() {
        return new IngredientDTO();
    }

    @Override
    public IngredientDTO createDTO(IngredientDTO ingredientDTO) {
        return restTemplate.postForObject(urlIngredients, ingredientDTO, IngredientDTO.class);
    }

    public IngredientDTO getIngredientById(Long id) {
        return restTemplate.getForObject(urlIngredients + "/" + id, IngredientDTO.class);
    }

    @Override
    public void updateDTO(IngredientDTO updatedIngredientDTO) {
        String url = urlIngredients + "/" + updatedIngredientDTO.getId();
        restTemplate.put(url, updatedIngredientDTO);
    }

    @Override
    public void deleteDTO(IngredientDTO ingredientDTO) {
        moveToRecycleBin(urlIngredients, ingredientDTO.getId(), ingredientDTO.getName());
    }

    @Override
    public Integer checkUniqueName(String name) {
        String url = urlIngredients + "/" + "checkUniqueName?name={name}";
        return restTemplate.getForObject(url, Integer.class, name);
    }

    @Override
    public List<String> getAllUnitOfMeasures(String ingredientName) {
        String url = urlIngredients + "/" + "getUnitOfMeasures?name={name}";
        return restTemplate.exchange(url, HttpMethod.GET,
                null, new ParameterizedTypeReference<List<String>> () {}, ingredientName).getBody();
    }

    @Override
    public List<IngredientDTO> searchName(String name) {
        return ingredients.stream()
                .filter(ingredientDTO -> ingredientDTO.getName().toLowerCase()
                        .contains(name.toLowerCase()))
                .collect(Collectors.toList());
    }
}
