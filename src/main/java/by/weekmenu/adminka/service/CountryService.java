package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.CountryDTO;

public interface CountryService extends CrudServiceClient<CountryDTO, Long>{

    Integer checkUniqueName(String name);
    Integer checkUniqueAlphaCode2(String alphaCode2);

}
