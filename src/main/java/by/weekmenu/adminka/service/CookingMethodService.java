package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.CookingMethodDTO;

public interface CookingMethodService extends CrudServiceClient<CookingMethodDTO, Integer> {

}