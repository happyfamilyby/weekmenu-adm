package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.RecipeDTO;

import java.math.BigDecimal;
import java.util.List;

public interface RecipeService extends CrudServiceClient<RecipeDTO, Long>,
        SearchService<RecipeDTO>{

    Integer checkUniqueName(String name);

}
