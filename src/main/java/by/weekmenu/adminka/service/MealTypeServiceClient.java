package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.MealTypeDTO;
import by.weekmenu.adminka.ui.util.AppConsts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Comparator;
import java.util.List;

@Service
public class MealTypeServiceClient implements MealTypeService {

    private final String urlMealTypes;
    private final RestTemplate restTemplate;

    @Autowired
    public MealTypeServiceClient(RestTemplate restTemplate, @Value("${weekmenu.server.url}") String baseUrl) {
        this.restTemplate = restTemplate;
        urlMealTypes = baseUrl + AppConsts.PAGE_MEAL_TYPE;
    }

    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Override
    public List<MealTypeDTO> getAllDTOs() {
        List<MealTypeDTO> sortedMealtypeDto = restTemplate.exchange(urlMealTypes, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<MealTypeDTO>>() {
                }).getBody();
        sortedMealtypeDto.sort(new Comparator<MealTypeDTO>() {
            @Override
            public int compare(MealTypeDTO mealTypeDTO1, MealTypeDTO mealTypeDTO2) {
                return mealTypeDTO1.getPriority().compareTo(mealTypeDTO2.getPriority());
            }
        });
        return sortedMealtypeDto;
    }

    @Override
    public MealTypeDTO createNewDTO() {
        return new MealTypeDTO();
    }

    @Override
    public MealTypeDTO createDTO(MealTypeDTO entityDTO) {
        return restTemplate.postForObject(urlMealTypes, entityDTO, MealTypeDTO.class);
    }

    @Override
    public void updateDTO(MealTypeDTO updatedDTO) {
        String url = urlMealTypes + "/" + updatedDTO.getId();
        restTemplate.put(url, updatedDTO);

    }

    @Override
    public void deleteDTO(MealTypeDTO entityDTO) {
        moveToRecycleBin(urlMealTypes, entityDTO.getId(), entityDTO.getName());
    }

    @Override
    public Integer checkMealTypeUniqueName(String name) {
        String url = urlMealTypes + "/" + "checkMealTypeUniqueName?name={name}";
        return restTemplate.getForObject(url, Integer.class, name);
    }

    @Override
    public Integer checkMealTypeUniquePriority(String priority) {
        String url = urlMealTypes + "/" + "checkMealTypeUniquePriority?priority={priority}";
        return restTemplate.getForObject(url, Integer.class, priority);
    }
}