package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.MenuDTO;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.util.SortingClass;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MenuServiceClient implements MenuService {

    private final String urlMenus;
    private final RestTemplate restTemplate;
    private List<MenuDTO> menus = new ArrayList<>();

    public MenuServiceClient(RestTemplate restTemplate,
                               @Value("${weekmenu.server.url}") String baseUrl) {
        this.urlMenus = baseUrl + AppConsts.PAGE_MENU;
        this.restTemplate = restTemplate;
    }

    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Override
    public List<MenuDTO> getAllDTOs() {
        menus = restTemplate.exchange(urlMenus, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<MenuDTO>>() {})
                .getBody();
        SortingClass.sortByWeeknumber(menus);
        return menus;
    }

    @Override
    public MenuDTO createNewDTO() {
        return new MenuDTO();
    }

    @Override
    public MenuDTO createDTO(MenuDTO entityDTO) {
        return restTemplate.exchange(urlMenus, HttpMethod.POST, new HttpEntity<>(entityDTO), MenuDTO.class)
                .getBody();
    }

    @Override
    public void updateDTO(MenuDTO updatedDTO) {
        String url = urlMenus + "/" + updatedDTO.getId();
        restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(updatedDTO), Void.class);
    }

    @Override
    public void deleteDTO(MenuDTO entityDTO) {
        if (entityDTO.getIsActive().equals(true)) {
            Button button = new Button("Закрыть");
            Label label = new Label("Меню '" + entityDTO.getName() + "' не может быть удалёно," +
                    " т.к. является активным");
            VerticalLayout layout = new VerticalLayout(label, button);
            Notification notification = new Notification(layout);
            notification.setPosition(Notification.Position.MIDDLE);
            notification.open();
            button.addClickListener(click -> notification.close());
        } else {
            restTemplate.delete(urlMenus + "/" + entityDTO.getId().toString());
            Notification.show("Меню '" + entityDTO.getName() + "' перемещёно в корзину");
        }
    }

    @Override
    public MenuDTO findMenuDTO(Long id) {
        String url = urlMenus + "/" + id.toString();
        return restTemplate.exchange(url, HttpMethod.GET, null, MenuDTO.class).getBody();
    }
}
