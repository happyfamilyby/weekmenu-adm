package by.weekmenu.adminka.service;

import java.util.List;

public interface SearchService<DTO> {

     List<DTO> searchName(String name);
}
