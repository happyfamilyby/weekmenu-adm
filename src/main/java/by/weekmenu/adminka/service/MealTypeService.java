package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.MealTypeDTO;

public interface MealTypeService extends CrudServiceClient<MealTypeDTO, Short> {

    Integer checkMealTypeUniqueName(String name);
    Integer checkMealTypeUniquePriority(String priority);
}