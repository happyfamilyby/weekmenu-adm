package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.IngredientCategoryDTO;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.util.SortingClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class IngredientCategoryServiceClient implements IngredientCategoryService {

    private final String urlIngredientCategories;
    private final RestTemplate restTemplate;

    @Autowired
    public IngredientCategoryServiceClient(RestTemplate restTemplate, @Value("${weekmenu.server.url}") String baseUrl) {
        this.restTemplate = restTemplate;
        urlIngredientCategories = baseUrl + AppConsts.PAGE_INGREDIENT_CATEGORY;
    }

    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Override
    public List<IngredientCategoryDTO> getAllDTOs() {
        List<IngredientCategoryDTO> list = restTemplate.exchange(urlIngredientCategories, HttpMethod.GET,
                null, new ParameterizedTypeReference<List<IngredientCategoryDTO>>() {
                }).getBody();
        SortingClass.sortByPriority(list);
        return list;
    }

    @Override
    public IngredientCategoryDTO createNewDTO() {
        return new IngredientCategoryDTO();
    }

    @Override
    public IngredientCategoryDTO createDTO(IngredientCategoryDTO entityDTO) {
        return restTemplate.postForObject(urlIngredientCategories, entityDTO, IngredientCategoryDTO.class);
    }

    @Override
    public void updateDTO(IngredientCategoryDTO updatedDTO) {
        String url = urlIngredientCategories + "/" + updatedDTO.getId();
        restTemplate.put(url, updatedDTO);

    }

    @Override
    public void deleteDTO(IngredientCategoryDTO entityDTO) {
        moveToRecycleBin(urlIngredientCategories, entityDTO.getId(), entityDTO.getName());
    }
}
