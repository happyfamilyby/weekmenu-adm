package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.RecipeCategoryDTO;

public interface RecipeCategoryService extends CrudServiceClient<RecipeCategoryDTO, Long> {

}
