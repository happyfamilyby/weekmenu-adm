package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.CurrencyDTO;

public interface CurrencyService extends CrudServiceClient<CurrencyDTO, Integer> {

    Integer checkUniqueName(String name);

    Integer checkUniqueCode(String code);
}