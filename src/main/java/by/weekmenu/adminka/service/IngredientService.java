package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.IngredientDTO;

import java.util.List;

public interface IngredientService extends CrudServiceClient<IngredientDTO, Long>,
        SearchService<IngredientDTO> {

    Integer checkUniqueName(String name);
    List<String> getAllUnitOfMeasures(String ingredientName);
}
