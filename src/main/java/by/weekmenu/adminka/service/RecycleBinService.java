package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.RecycleBinDTO;

import java.util.List;

public interface RecycleBinService {

    List<RecycleBinDTO> getAllBins();
    void delete(Long id);
    void restore(Long id);
}
