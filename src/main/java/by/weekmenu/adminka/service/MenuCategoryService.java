package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.MenuCategoryDTO;

public interface MenuCategoryService extends CrudServiceClient<MenuCategoryDTO, Integer> {

}
