package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.MenuCategoryDTO;
import by.weekmenu.adminka.ui.util.AppConsts;
import by.weekmenu.adminka.ui.util.SortingClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class MenuCategoryServiceClient implements MenuCategoryService {

    private final String urlMenuCategories;
    private final RestTemplate restTemplate;

    @Autowired
    public MenuCategoryServiceClient(RestTemplate restTemplate, @Value("${weekmenu.server.url}") String baseUrl) {
        this.restTemplate = restTemplate;
        urlMenuCategories = baseUrl + AppConsts.PAGE_MENUCATEGORY;
    }

    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Override
    public List<MenuCategoryDTO> getAllDTOs() {
        List<MenuCategoryDTO> list = restTemplate.exchange(urlMenuCategories, HttpMethod.GET,
                null, new ParameterizedTypeReference<List<MenuCategoryDTO>>() {
                }).getBody();
        SortingClass.sortByPriority(list);
        return list;
    }

    @Override
    public MenuCategoryDTO createNewDTO() {
        return new MenuCategoryDTO();
    }

    @Override
    public MenuCategoryDTO createDTO(MenuCategoryDTO entityDTO) {
        return restTemplate.postForObject(urlMenuCategories, entityDTO, MenuCategoryDTO.class);
    }

    @Override
    public void updateDTO(MenuCategoryDTO updatedDTO) {
        String url = urlMenuCategories + "/" + updatedDTO.getId();
        restTemplate.put(url, updatedDTO);

    }

    @Override
    public void deleteDTO(MenuCategoryDTO entityDTO) {
        moveToRecycleBin(urlMenuCategories, entityDTO.getId(), entityDTO.getName());
    }
}
