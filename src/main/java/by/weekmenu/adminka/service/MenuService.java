package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.MenuDTO;

public interface MenuService extends CrudServiceClient<MenuDTO, Long>{

        MenuDTO findMenuDTO(Long id);
}
