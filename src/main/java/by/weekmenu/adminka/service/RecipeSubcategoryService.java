package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.RecipeSubcategoryDTO;

public interface RecipeSubcategoryService extends CrudServiceClient<RecipeSubcategoryDTO, Long> {

}
