package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.RecipeDTO;
import by.weekmenu.adminka.ui.util.AppConsts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RecipeServiceClient implements RecipeService {

    private final String urlRecipes;
    private final RestTemplate restTemplate;
    private List<RecipeDTO> recipes = new ArrayList<>();

    public RecipeServiceClient(RestTemplate restTemplate,
                               @Value("${weekmenu.server.url}") String baseUrl) {
        this.urlRecipes = baseUrl + AppConsts.PAGE_RECIPE;
        this.restTemplate = restTemplate;
    }

    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Override
    public List<RecipeDTO> getAllDTOs() {
        this.recipes = restTemplate.exchange(urlRecipes, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<RecipeDTO>>() {})
                .getBody();
        return this.recipes;
    }

    @Override
    public RecipeDTO createNewDTO() {
        return new RecipeDTO();
    }

    @Override
    public RecipeDTO createDTO(RecipeDTO entityDTO) {
        return restTemplate
                .exchange(urlRecipes, HttpMethod.POST, new HttpEntity<>(entityDTO), RecipeDTO.class)
                .getBody();

    }

    @Override
    public void updateDTO(RecipeDTO updatedDTO) {
        String url = urlRecipes + "/" + updatedDTO.getId();
        updatedDTO.setCalories(updatedDTO.getCalories().setScale(0));
        restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(updatedDTO), Void.class);
    }

    @Override
    public void deleteDTO(RecipeDTO entityDTO) {
        moveToRecycleBin(urlRecipes, entityDTO.getId(), entityDTO.getName());
    }

    @Override
    public Integer checkUniqueName(String name) {
        String url = urlRecipes+ "/" + "checkUniqueName?name={name}";
        return restTemplate.exchange(url, HttpMethod.GET, null, Integer.class, name).getBody();
    }

    @Override
    public List<RecipeDTO> searchName(String name) {
        return this.recipes.stream()
                .filter(recipeDTO -> recipeDTO.getName().toLowerCase()
                        .contains(name.toLowerCase()))
                .collect(Collectors.toList());
    }
}
