package by.weekmenu.adminka.service;

import by.weekmenu.adminka.dto.IngredientCategoryDTO;

public interface IngredientCategoryService extends CrudServiceClient<IngredientCategoryDTO, Integer> {

}
