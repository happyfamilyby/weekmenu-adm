package by.weekmenu.adminka.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class CurrencyDTO {

    private Integer id;
    private String name;
    private String code;
}
