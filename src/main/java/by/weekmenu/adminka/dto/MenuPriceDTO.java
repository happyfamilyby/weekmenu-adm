package by.weekmenu.adminka.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class MenuPriceDTO {

    private String regionName;
    private String menuName;
    private String priceValue;
    private String currencyCode;
}
