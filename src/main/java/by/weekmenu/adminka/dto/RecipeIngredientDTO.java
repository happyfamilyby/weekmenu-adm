package by.weekmenu.adminka.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class RecipeIngredientDTO {

    private String ingredientName;
    private String quantity;
    private String unitOfMeasureShortName;
}