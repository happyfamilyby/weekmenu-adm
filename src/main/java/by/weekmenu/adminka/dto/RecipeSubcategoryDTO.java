package by.weekmenu.adminka.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class RecipeSubcategoryDTO extends CategorySuperDTO{

    private Long id;

}
