package by.weekmenu.adminka.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class MenuCategoryDTO extends CategorySuperDTO{

    private Integer id;
    private String imageLink;

}
