package by.weekmenu.adminka.dto;

import lombok.Data;

@Data
public class CategorySuperDTO {

    private String name;
    private String priority;

}
