package by.weekmenu.adminka.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class MealTypeDTO {

    private Short id;
    private String name;
    private String priority;
}
