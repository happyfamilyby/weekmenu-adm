package by.weekmenu.adminka.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class RegionDTO {

    private Long id;
    private String name;
    private String countryName;
    private String countryCurrencyCode;
}
