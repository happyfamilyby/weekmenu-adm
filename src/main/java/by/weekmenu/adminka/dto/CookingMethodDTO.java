package by.weekmenu.adminka.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CookingMethodDTO extends CategorySuperDTO{

    private Integer id;

}
