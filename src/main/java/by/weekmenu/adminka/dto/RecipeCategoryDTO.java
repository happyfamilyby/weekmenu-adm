package by.weekmenu.adminka.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class RecipeCategoryDTO extends CategorySuperDTO{

    private Long id;

}
