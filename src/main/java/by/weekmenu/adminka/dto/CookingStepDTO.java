package by.weekmenu.adminka.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CookingStepDTO {

    private Integer id;
    private String priority;
    private String description;
    private String imageLink;
}