package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber"},
        features = "src/test/resources/features/001_Успешное_заведение_ингредиента",
        tags = "not @ignore",
        glue = "stepdefs")
public class IngredientsRunner {

}
