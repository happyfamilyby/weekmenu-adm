package stepdefs;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverProvider;
import cucumber.api.java.ru.Если;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URI;

import static com.codeborne.selenide.Selenide.clearBrowserCookies;
import static com.codeborne.selenide.Selenide.open;

public class CommonSteps {
    boolean firstly = true;

    @Если("Анфиса переходит на страницу {string} в браузере {string}")
    public void анфисаПереходитНаСтраницу(String pageName, String browserName) {
        open(pageName);
    }

    @Если("Анфиса переходит на страницу {string}")
    public void анфисаПереходитНаСтраницу(String pageName) throws Exception {
        String token = "BJKwXU0RjXyTLom";

//        Configuration.startMaximized = true;
//        //Configuration.headless = true;
//
//        //Configuration.browser = "chrome";
//        ;
//        Configuration.remote = CustomProvider.class.getName();

        if(firstly) setUp();

        switch(pageName){
            case "Ингредиенты":
                clearBrowserCookies();
                open("/ingredients/" + token);
                break;
            case "Корзина":
                open("/recycleBin/" + token);
                break;
            case "Рецепты":
                open("/recipes/" + token);
                break;
            default:
                throw new Exception();
        }
    }

    void setUp() {
        Configuration.driverManagerEnabled = false;
        Configuration.browser = CustomProvider.class.getName();
        Configuration.timeout = 5000;
        Configuration.baseUrl = "http://178.124.206.44:8080/weekmenu_admin";
        Configuration.startMaximized = true;
    }

    public static class CustomProvider implements WebDriverProvider {

        @Override
        public WebDriver createDriver(DesiredCapabilities capabilities) {
            DesiredCapabilities browser = new DesiredCapabilities();
            browser.setBrowserName("chrome");
            browser.setVersion("78");
            browser.setCapability("enableVNC", true);
            browser.merge(capabilities);
            try {
                return new RemoteWebDriver(URI.create("http://178.124.206.44:4444/wd/hub").toURL(), browser);
            } catch (final MalformedURLException e) {
                throw new RuntimeException("Unable to create driver", e);
            }
        }
    }
}
