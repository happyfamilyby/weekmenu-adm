package stepdefs;

import cucumber.api.java.ru.Если;
import cucumber.api.java.ru.И;
import cucumber.api.java.ru.Пусть;
import cucumber.api.java.ru.То;
import io.cucumber.datatable.DataTable;
import org.junit.Assert;
import page_objects.AdminPage;

import java.util.List;
import java.util.Map;

public class IngredientsSteps {
    @И("должнен отображаться элемент типа {string} с названием {string}")
    public void наСтраницеДолжненОтображатьсяЭлементТипаСНазванием(String typeOfElement, String nameOfElement) {
        Assert.assertTrue(AdminPage.CheckElementIsDisplayed(nameOfElement));
    }

    @То("должна отображаться следующая запись:")
    @Пусть("отображается следующая запись:")
    public void должнаОтображатьсяСледующаяЗапись(DataTable table) {
        for (List<String> row:
             table.cells()) {
            Assert.assertTrue(AdminPage.CheckNoteIsDisplayed(row));
        }
    }

    @Если("Анфиса нажимает кнопку {string}")
    public void анфисаНажимаетКнопку(String elementName) throws InterruptedException {
        AdminPage.ClickClickableElement(elementName);
    }

    @И("Анфиса открывает вкладку {string}")
    public void анфисаОткрываетВкладку(String tabName) throws InterruptedException {
        AdminPage.ClickClickableElement(tabName);
    }


    @То("форма {string} должна закрыться")
    public void формаДолжнаЗакрыться(String formName) {
        Assert.assertFalse(AdminPage.CheckElementIsDisplayed(formName));
    }

    @И("должна отображаться надпись: {string}")
    public void должнаОтображатьсяНадпись(String labelName) {
        Assert.assertTrue(AdminPage.CheckLabel(labelName));
    }

    @И("Анфиса заполняет форму следующими значениями:")
    public void анфисаЗаполняетФормуСледующимиЗначениями(Map<String, String> note) {
        for (Map.Entry<String, String> entry:
             note.entrySet()) {
            AdminPage.SetValueInField(entry.getKey(), entry.getValue());
        }
    }

    @Если("Анфиса открывает запись:")
    public void анфисаОткрываетЗапись(DataTable table) {
        for (List<String> row:
                table.cells()) {
            AdminPage.OpenNote(row);
        }
    }

    @Если("Анфиса меняет значение на {string} в поле {string}")
    public void анфисаМеняетЗначениеНаВПоле(String newValue, String fieldName) {
        AdminPage.SetValueInField(fieldName, newValue);
    }

    @То("должна отобразиться форма {string} со следующими значениями:")
    public void должнаОтобразитьсяФормаСоСледующимиЗначениями(String formName, Map<String,String> values) {
        for (Map.Entry<String, String> currentValue:
             values.entrySet()) {
            Assert.assertTrue(AdminPage.CheckValueInField(currentValue.getKey(), currentValue.getValue()));
        }
    }


    @То("должны отображаться следующие вкладки:")
    public void должныОтображатьсяСледующиеВкладки(List<String> tabs) {
        for (String tab:
             tabs) {
            Assert.assertTrue(AdminPage.CheckElementIsDisplayed(tab));
        }
    }

    @То("должна отображатья только следующая запись:")
    public void должнаОтображатьяТолькоСледующаяЗапись(DataTable table) {
        for (List<String> row:
                table.cells()) {
            Assert.assertTrue(AdminPage.CheckNoteIsDisplayed(row));
        }
    }

    @И("Анфиса добавляет новую единицу измерения {string} с эквивалентом в гр. {string}")
    public void анфисаДобавляетНовуюЕдиницуИзмеренияСЭквивалентомВГр(String unitOfMeasure, String equivalent) {
        AdminPage.AddUnitOfMeasureToIngredient(unitOfMeasure, equivalent);
    }

    @То("должен отображаться следующих компонентов для удаления:")
    public void долженОтображатьсяСледующихКомпонентовДляУдаления(List<String> components) {
        for (String comp:
             components) {
            Assert.assertTrue(AdminPage.CheckElementIsDisplayed(comp));
        }
    }

    @То("должно появится окно {string}")
    public void должноПоявитсяОкно(String windowName) {
        Assert.assertTrue(AdminPage.CheckElementIsDisplayed(windowName));
    }

    @То("вся корзина должна остаться пуста")
    public void всяКорзинаДолжнаОстатьсяПуста() {
        Assert.assertTrue(AdminPage.CheckElementIsDisplayed("Пустая корзина"));
    }

    @Если("Анфиса удаляет запись {string}")
    public void анфисаУдаляетЗапись(String note) {
        AdminPage.DeleteNote(note);
    }

    @То("следующие компоненты должны быть удалены:")
    public void следующиеКомпонентыДолжныБытьУдалены(List<String> components) {
        for (String component:
             components) {
            Assert.assertFalse(AdminPage.CheckElementIsDisplayed(component));
        }
    }

    @И("Анфиса добавляет картинку {string}")
    public void анфисаДобавляетКартинку(String imageName) {
        AdminPage.AddImage(imageName);
    }
}


