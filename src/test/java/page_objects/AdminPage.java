package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;

import static com.codeborne.selenide.Selenide.*;

public class AdminPage {
    private AdminPage() {}

    private static By unitsOfMeasureTab = By.xpath("//a[contains(.,'Ед. измерения')]");
    private static By addUnitOfMeasure = By.id("AddButton");
    private static By formFullName = By.id("FullNameField");
    private static By formShortName = By.id("ShortNameField");
    private static By saveNote = By.id("SaveButton");
    private static By cancelNote = By.id("SaveButton");
    private static By deleteNote = By.id("DeleteButton");
    private static By formOfAddingUnit = By.id("UnitOfMeasureForm");

    private static By currencyTab = By.xpath("//a[contains(.,'Валюты')]");
    private static By filter = By.xpath("//vaadin-combo-box[@label='Фильтр']");
    private static By currencyName = By.id("CurrencyNameField");
    private static By currencyCode = By.id("CurrencyCodeField");
    private static By currencySymbol = By.id("CurrencySymbolField");
    private static By currencyForm = By.id("CurrencyForm");

    private static By countriesTab = By.xpath("//a[contains(.,'Страны')]");
    private static By countryName = By.id("NameField");
    private static By countryCode = By.id("CodeField");
    private static By countryCurrency = By.id("CurrencyField");
    private static By countriesForm = By.id("CountryForm");

    private static By regionsTab = By.xpath("//a[contains(.,'Регионы')]");
    private static By regionNameField = By.id("RegionNameField");
    private static By regionCountryField = By.id("RegionCountryField");
    private static By regionsForm = By.id("RegionForm");

    private static By ingredientsTab = By.xpath("//a[contains(.,'Ингредиенты')]");
    private static By ingredientNameField = By.id("NameField");
    private static By ingredientCaloriesField = By.id("CaloriesField");
    private static By ingredientProteinsField = By.id("ProteinsField");
    private static By ingredientFatsField = By.id("FatsField");
    private static By ingredientCarbsField = By.id("CarbsField");
    private static By ingredientUnitOfMeasureFullNameField = By.xpath("//h5[contains(.,'Единицы измерения')]/following::vaadin-vertical-layout[1]/vaadin-vertical-layout[2]/vaadin-horizontal-layout/vaadin-combo-box");
    private static By ingredientEquivalentField = By.xpath("//h5[contains(.,'Единицы измерения')]/following::vaadin-vertical-layout[1]/vaadin-vertical-layout[2]/vaadin-horizontal-layout/vaadin-text-field");
    private static By addEquivalentMeasure = By.id("AddIngredientUnitOfMeasureButton");
    private static By deleteEquivalentMeasure = By.id("DeleteIngredientUnitOfMeasureButton");
    private static By ingredientRegionNameField = By.id("RegionNameField");
    private static By ingredientPriceValueField = By.id("PriceValueField");
    private static By ingredientCurrencyCodeField = By.id("CurrencyCodeField");
    private static By ingredientQuantityField = By.id("QuantityField");
    private static By ingredientUnitOfMeasureField = By.id("UnitOfMeasureField");
    private static By addIngredientPriceButton = By.id("AddIngredientPriceButton");
    private static By deleteIngredientPriceButton = By.id("DeleteIngredientPriceButton");
    private static By ingredientForm = By.id("IngredientForm");

    private static By deleteForeverForm = By.xpath("//vaadin-vertical-layout/h4[contains(.,'Удалить навсегда?')]");
    private static By deleteElement = By.xpath("//vaadin-vertical-layout/h4[contains(.,'Удалить навсегда?')]/following::vaadin-button[1]");
    private static By deleteAllNotes = By.id("RecycleBinDeleteAllButton");
    private static By fullyCleaningRecycleBin = By.xpath("//vaadin-vertical-layout/h4[contains(.,'Полностью очистить корзину?')]");
    private static By deleteAllNotesOnForm = By.xpath("//vaadin-vertical-layout/h4[contains(.,'Полностью очистить корзину?')]/following::vaadin-button[1]");

    private static By cookingMethodsTab = By.xpath("//a[contains(.,'Способы приготовления')]");
    private static By cookingMethodField = By.id("CookingMethodNameField");

    private static By subcategoriesTab = By.xpath("//a[contains(.,'Подкатегории рецептов')]");
    private static By subcategoryField = By.id("RecipeSubcategoryNameField");

    private static By categoriesTab = By.xpath("//a[contains(.,'Категории рецептов')]");
    private static By categoryField = By.id("RecipeCategoryNameField");

    private static By receiptsTab = By.xpath("//a[contains(.,'Рецепты')]");
    private static By receiptName = By.id("RecipeNameField");
    private static By quantityPortions = By.id("RecipePortionsField");
    private static By prodTime = By.id("RecipeCookingTimeField");
    private static By preTime = By.id("RecipePreparingTimeField");
    private static By receiptSource = By.id("RecipeSourceField");
    private static By cookingMethod = By.id("RecipeCookingMethodNameField");
    private static By categoryName = By.xpath("//vaadin-horizontal-layout/multiselect-combo-box[1]");
    private static By subcategoryName = By.xpath("//vaadin-horizontal-layout/multiselect-combo-box[2]");
    private static By ingredientName = By.id("IngredientNameField");
    private static By quantityIngredient = By.id("QuantityField");
    private static By unitsOfMeasureName = By.id("UnitOfMeasureNameField");
    private static By firstStepNumber = By.id("PriorityField");
    private static By firstStepDescription = By.id("DescriptionField");
    private static By receiptPhoto = By.xpath("//vaadin-dialog-overlay/flow-component-renderer/div/vaadin-vertical-layout/vaadin-horizontal-layout[1]/vaadin-horizontal-layout/vaadin-upload");
    private static By receiptFirstStepPhoto = By.xpath("//vaadin-dialog-overlay/flow-component-renderer/div/vaadin-vertical-layout/vaadin-vertical-layout[2]/vaadin-vertical-layout/vaadin-horizontal-layout/vaadin-horizontal-layout/vaadin-horizontal-layout/vaadin-upload");

    private static By plus = By.id("AddCookingStepButton");

    public static boolean CheckElementIsDisplayed(String nameOfElement) {
        switch (nameOfElement) {
            case "Добавить":
                return WaitUntilElementWillBeDisplayed($(addUnitOfMeasure));
            case "Фильтр":
                return WaitUntilElementWillBeDisplayed($(filter));
            case "Валюта":
                return WaitUntilElementWillBeDisplayed($(currencyForm));
            case "Страны":
                return WaitUntilElementWillBeDisplayed($(countriesTab));
            case "Регионы":
                return WaitUntilElementWillBeDisplayed($(regionsTab));
            case "Ингредиент":
                return WaitUntilElementWillBeDisplayed($(ingredientForm));
            case "Сохранить":
                return WaitUntilElementWillBeDisplayed($(saveNote));
            case "Отменить":
                return WaitUntilElementWillBeDisplayed($(cancelNote));
            case "Удалить":
                return WaitUntilElementWillBeDisplayed($(deleteNote));
            case "Единица измерения":
                return WaitUntilElementWillBeDisplayed($(formOfAddingUnit));
            case "Ингредиенты":
                return WaitUntilElementWillBeDisplayed($(ingredientsTab));
            case "Ед. измерения":
                return WaitUntilElementWillBeDisplayed($(unitsOfMeasureTab));
            case "Валюты":
                return WaitUntilElementWillBeDisplayed($(currencyTab));
            case "Удалить навсегда?":
                return WaitUntilElementWillBeDisplayed($(deleteForeverForm));
            case "Полностью очистить корзину?":
                return WaitUntilElementWillBeDisplayed($(fullyCleaningRecycleBin));
            default:
                refresh();
                return $(By.xpath("//vaadin-grid-cell-content[contains(.,'" + nameOfElement + "')]")).isDisplayed();
        }


    }

    private static boolean WaitUntilElementWillBeDisplayed(WebElement element) {
        Wait().until(c -> element.isDisplayed());
        return element.isDisplayed();
    }

    public static void ClickClickableElement(String elementName) {
        switch (elementName){
            case "Ед. измерения":
                $(unitsOfMeasureTab).click();
                break;
            case "Валюты":
                $(currencyTab).click();
                break;
            case "Страны":
                $(countriesTab).click();
                break;
            case "Регионы":
                $(regionsTab).click();
                break;
            case "Ингредиенты":
                $(ingredientsTab).click();
                break;
            case "Добавить":

                $(addUnitOfMeasure).click();
                break;
            case "Сохранить":
                actions().moveToElement($(saveNote)).click($(saveNote)).perform();
                break;
            case "Отменить":
                $(cancelNote).click();
                break;
            case "Удалить":
                actions().moveToElement($(deleteNote)).click($(deleteNote)).perform();
                break;
            case "Удалить элемент":
                $(deleteElement).click();
                break;
            case "Удалить всё":
                $(deleteAllNotes).click();
                break;
            case "Удалить все":
                $(deleteAllNotesOnForm).click();
                break;
            case "Способы приготовления":
                $(cookingMethodsTab).click();
                break;
            case "Подкатегории рецептов":
                $(subcategoriesTab).click();
                break;
            case "Категории рецептов":
                $(categoriesTab).click();
                break;
            case "Рецепты":
                $(receiptsTab).click();
                break;
            case "Плюс":
                $(plus).click();
                break;
            default:
                throw new NotFoundException();
        }
    }

    public static void SetValueInField(String key, String value) {
        WebElement currentElement = null;

        switch (key){
            case "Полное название":
                currentElement = $(formFullName);
                break;
            case "Сокращённое название":
                currentElement = $(formShortName);
                break;
            case "Название валюты":
                currentElement = $(currencyName);
                break;
            case "Код валюты":
                currentElement = $(currencyCode);
                break;
            case "Символ валюты":
                currentElement = $(currencySymbol);
                break;
            case "Название страны":
                currentElement = $(countryName);
                break;
            case "Код страны":
                currentElement = $(countryCode);
                break;
            case "Валюта страны":
                currentElement = $(countryCurrency);
                SetValueInComboBox(currentElement, value);
                return;
            case "Название региона":
                currentElement = $(regionNameField);
                break;
            case "Страна региона":
                currentElement = $(regionCountryField);
                SetValueInComboBox(currentElement, value);
                return;
            case "Название":
                currentElement = $(ingredientNameField);
                break;
            case "Ккал":
                currentElement = $(ingredientCaloriesField);
                break;
            case "Белки":
                currentElement = $(ingredientProteinsField);
                break;
            case "Жиры":
                currentElement = $(ingredientFatsField);
                break;
            case "Углеводы":
                currentElement = $(ingredientCarbsField);
                break;
            case "Единица измерения":
                currentElement = $(ingredientUnitOfMeasureField);
                break;
            case "Регион":
                currentElement = $(ingredientRegionNameField);
                SetValueInComboBox(currentElement, value);
                return;
            case "Кол-во":
                currentElement = $(ingredientQuantityField);
                break;
            case "Ед. измерения":
                currentElement = $(ingredientUnitOfMeasureField);
                SetValueInComboBox(currentElement, value);
                $(addIngredientPriceButton).click();
                return;
            case "Цена":
                currentElement = $(ingredientPriceValueField);
                break;
            case "Валюта":
                currentElement = $(ingredientCurrencyCodeField);
                break;
            case "Способ приготовления рецепта":
                currentElement = $(cookingMethodField);
                break;
            case "Подкатегория рецепта":
                currentElement = $(subcategoryField);
                break;
            case "Категория рецепта":
                currentElement = $(categoryField);
                break;
            case "Рецепт":
                currentElement = $(receiptName);
                break;
            case "Кол-во порций":
                currentElement = $(quantityPortions);
                break;
            case "Время приготовления":
                currentElement = $(prodTime);
                break;
            case "Время подготовки":
                currentElement = $(preTime);
                break;
            case "Источник рецепта":
                currentElement = $(receiptSource);
                break;
            case "Способ приготовления":
                currentElement = $(cookingMethod);
                SetValueInComboBox(currentElement, value);
                return;
            case "Категории":
                currentElement = $(categoryName);
                SetValueInMultiselect(GetInputInMultiselect(currentElement), value);
                return;
            case "Подкатегории":
                currentElement = $(subcategoryName);
                SetValueInMultiselect(GetInputInMultiselect(currentElement), value);
                return;
            case "Ингредиент":
                currentElement = $(ingredientName);
                SetValueInComboBox(currentElement, value);
                return;
            case "Кол-во ингредиента":
                currentElement = $(quantityIngredient);
                break;
            case "Ед. измерения для количества":
                currentElement = $(unitsOfMeasureName);
                SetValueInComboBox(currentElement, value);
                return;
            case "Шаг":
                currentElement = $(firstStepNumber);
                break;
            case "Описание шага":
                currentElement = $(firstStepDescription);
                SetValueInTextArea(currentElement, value);
                return;
            default:
                throw new NotFoundException();
        }

        SetValueInShadowField(currentElement, value);
    }

    private static void SetValueInTextArea(WebElement currentElement, String value) {
        WebElement myElement = GetShadowRootOfElement(currentElement).findElement(By.cssSelector("textarea"));

        myElement.click();
        myElement.clear();
        myElement.sendKeys(value);
    }

    private static void SetValueInMultiselect(WebElement currentElement, String value) {
        SetValueInShadowField(currentElement, value);
        currentElement.sendKeys(Keys.ENTER);
    }

    private static WebElement GetInputInMultiselect(WebElement currentElement) {
        return GetShadowRootOfElement(GetShadowRootOfElement(currentElement).findElement(By.cssSelector("multiselect-combo-box-input"))).findElement(By.cssSelector("vaadin-text-field"));
    }

    private static void SetValueInShadowField(WebElement currentElement, String value) {
        WebElement myElement = GetShadowRootOfElement(currentElement).findElement(By.cssSelector("input"));

        myElement.clear();
        myElement.sendKeys(value);
    }

    private static WebElement GetShadowRootOfElement(WebElement currentElement) {
        return executeJavaScript("return arguments[0].shadowRoot", currentElement);
    }

    private static void SetValueInComboBox(WebElement currentElement, String value) {
        WebElement myElement = GetSecondShadowRootInComboBox(currentElement).findElement(By.cssSelector("input"));

        myElement.clear();
        myElement.sendKeys(value);
        myElement.sendKeys(Keys.DOWN);
        myElement.sendKeys(Keys.ENTER);

        Wait().until(c -> myElement.getAttribute("value").equals(value));
    }

    public static boolean CheckValueInField(String key, String value) {
        switch (key) {
            case "Полное название":
                return $(formFullName).getValue().equals(value);
            case "Сокращённое название":
                return $(formShortName).getValue().equals(value);
            case "Название валюты":
                return $(currencyName).getValue().equals(value);
            case "Код валюты":
                return $(currencyCode).getValue().equals(value);
            case "Название страны":
                return $(countryName).getValue().equals(value);
            case "Код страны":
                return $(countryCode).getValue().equals(value);
            case "Валюта страны":
                return GetValueFromCombobox($(countryCurrency)).equals(value);
            case "Название региона":
                return $(regionNameField).getValue().equals(value);
            case "Страна региона":
                return GetValueFromCombobox($(regionCountryField)).equals(value);
            case "Название":
                return $(ingredientNameField).getValue().equals(value);
            case "Ккал":
                return $(ingredientCaloriesField).getValue().equals(value);
            case "Белки":
                return $(ingredientProteinsField).getValue().equals(value);
            case "Жиры":
                return $(ingredientFatsField).getValue().equals(value);
            case "Углеводы":
                return $(ingredientCarbsField).getValue().equals(value);
            case "Регион":
                return GetValueFromCombobox($(ingredientRegionNameField)).equals(value);
            case "Цена":
                return $(ingredientPriceValueField).getValue().equals(value);
            case "Кол-во":
                return $(ingredientQuantityField).getValue().equals(value);
            case "Ед. измерения":
                return GetValueFromCombobox($(ingredientUnitOfMeasureField)).equals(value);
            case "Способ приготовления рецепта":
                return $(cookingMethodField).getValue().equals(value);
            case "Подкатегория рецепта":
                return $(subcategoryField).getValue().equals(value);
            case "Категория рецепта":
                return $(categoryField).getValue().equals(value);
            case "Рецепт":
                return $(receiptName).getValue().equals(value);
            case "Кол-во порций":
                return $(quantityPortions).getValue().contains(value);
            case "Время приготовления":
                return $(prodTime).getValue().equals(value);
            case "Время подготовки":
                return $(preTime).getValue().equals(value);
            case "Источник рецепта":
                return $(receiptSource).getValue().equals(value);
            case "Способ приготовления":
                return GetValueFromCombobox($(cookingMethod)).equals(value);
            case "Ингредиент":
                return GetValueFromCombobox($(ingredientName)).equals(value);
            case "Кол-во ингредиента":
                return $(quantityIngredient).getValue().equals(value);
            case "Ед. измерения для количества":
                return GetValueFromCombobox($(unitsOfMeasureName)).equals(value);
            case "Шаг":
                return $(firstStepNumber).scrollIntoView(true).getValue().equals(value);
            case "Описание шага":
                return $(firstStepDescription).scrollIntoView(true).getValue().contains(value);
            case "Категории":
                return CheckValueInMultiselect($(categoryName), value);
            case "Подкатегории":
                return CheckValueInMultiselect($(subcategoryName), value);
            default:
                return false;
        }
    }

    private static boolean CheckValueInMultiselect(WebElement currentElement, String value) {
        WebElement newElement = GetShadowRootOfElement(GetShadowRootOfElement(currentElement).findElement(By.cssSelector("multiselect-combo-box-input")));
        List<WebElement> listOfSelectesItems = newElement.findElement(By.cssSelector("#tokens")).findElements(By.cssSelector("div"));

        for (WebElement currentOption:
             listOfSelectesItems) {
            if(currentOption.getText().equals(value)) return true;
        }

        return false;
    }

    private static String GetValueFromCombobox(WebElement currentElement) {
        WebElement myElement = GetSecondShadowRootInComboBox(currentElement).findElement(By.cssSelector("div > div:nth-child(2) > slot:nth-child(2) > input"));
        return myElement.getAttribute("value");
    }

    private static WebElement GetSecondShadowRootInComboBox(WebElement element) {
        WebElement shadowRoot = executeJavaScript("return arguments[0].shadowRoot", element);
        return executeJavaScript("return arguments[0].shadowRoot", shadowRoot.findElement(By.id("input")));
    }

    public static boolean CheckLabel(String labelName) {
        Wait().until(c -> GetLabel(labelName).isDisplayed());
        return GetLabel(labelName).isDisplayed();
    }

    public static boolean CheckNoteIsDisplayed(List<String> noteComponents) {
        Wait().until(c -> GetNote(noteComponents).isDisplayed());
        return GetNote(noteComponents).isDisplayed();
    }

    private static WebElement GetNote(List<String> noteComponents){
        String resultingString = "//vaadin-grid-cell-content[contains(.,'";
        for (String component:
             noteComponents) {
            resultingString += component;

            if(noteComponents.get(noteComponents.size() - 1).equals(component) || noteComponents.size() == 1){
                resultingString += "')]";
            } else {
                resultingString += "')]/following::vaadin-grid-cell-content[contains(.,'";
            }
        }

        return $(By.xpath(resultingString));
    }

    private static WebElement GetLabel(String labelName){
        return $(By.xpath("//h1[contains(.,'" + labelName + "')]"));
    }

    public static void OpenNote(List<String> noteComponents) {
        GetNote(noteComponents).click();
    }

    public static void AddUnitOfMeasureToIngredient(String unitOfMeasure, String equivalent) {
        $(addEquivalentMeasure).click();

        WebElement currentElement = $(ingredientUnitOfMeasureFullNameField);

        SetValueInComboBox(currentElement, unitOfMeasure);

        $(ingredientEquivalentField).sendKeys(equivalent);
    }

    public static void DeleteNote(String note) {
        WebElement noteElement = $(By.xpath("//vaadin-grid-cell-content[contains(.,'" +
                note + "')]/following::vaadin-grid-cell-content[3]/flow-component-renderer/vaadin-horizontal-layout/vaadin-button[@id='RecycleBinDeleteButton']"));

        noteElement.click();
    }

    public static void AddImage(String imageName) {
        switch (imageName){
            case "Фото рецепта":
                WebElement inputForPhoto = GetShadowRootOfElement($(receiptPhoto)).findElement(By.cssSelector("input"));
                File file = new File(Paths.get("").toAbsolutePath().toString() + "/src/test/resources/images", "grechka.jpg");
                inputForPhoto.sendKeys(file.getAbsolutePath());
                break;
            case "1 шаг рецепта":
                WebElement inputForFirstStepPhoto = GetShadowRootOfElement($(receiptFirstStepPhoto)).findElement(By.cssSelector("input"));
                File file2 = new File(Paths.get("").toAbsolutePath().toString() + "/src/test/resources/images", "svarite_grechku.png");
                inputForFirstStepPhoto.sendKeys(file2.getAbsolutePath());
                break;
            default:
                return;
        }
    }
}
