#language: ru
#encoding: utf-8

Функция: Добавление \ Редактирование ингредиентов
  Чтобы работать со Справочником ингредиентов
  Анфисе Администрирующей необходимо добавлять и редактировать ингредиенты

  Сценарий: Переход на страницу справочника ингредиентов
    Если Анфиса открывает вкладку "Ингредиенты"
    То должна отображаться надпись: "Пока ничего не добавлено"
    И должнен отображаться элемент типа "Кнопка" с названием "Добавить"

  Сценарий: Добавление ингредиента
    Если Анфиса нажимает кнопку "Добавить"
    И Анфиса добавляет новую единицу измерения "Килограмм" с эквивалентом в гр. "1000"
    И Анфиса заполняет форму следующими значениями:
      | Название      | Гречневая крупа ядрица |
      | Ккал          | 308                    |
      | Белки         | 12,6                   |
      | Жиры          | 3,3                    |
      | Углеводы      | 55,1                   |
      | Регион        | Брчко                  |
      | Цена          | 2,99                   |
      | Кол-во        | 0,5                    |
      | Ед. измерения | Килограмм              |
    И Анфиса нажимает кнопку "Сохранить"
    То должна отображаться следующая запись:
      | Гречневая крупа ядрица | 308 / 12,6 / 3,3 / 55,1 |

  Сценарий: Редактирование ингредиента
    Пусть отображается следующая запись:
      | Гречневая крупа ядрица | 308 / 12,6 / 3,3 / 55,1 |
    Если Анфиса открывает запись:
      | Гречневая крупа ядрица | 308 / 12,6 / 3,3 / 55,1 |
    То должна отобразиться форма "Ингредиент" со следующими значениями:
      | Название      | Гречневая крупа ядрица |
      | Ккал          | 308,0                  |
      | Белки         | 12,6                   |
      | Жиры          | 3,3                    |
      | Углеводы      | 55,1                   |
      | Регион        | Брчко                  |
      | Цена          | 2,99                   |
      | Кол-во        | 0,5                    |
      | Ед. измерения | Килограмм              |
    Если Анфиса меняет значение на "57,1" в поле "Углеводы"
    И Анфиса нажимает кнопку "Сохранить"
    То должна отображаться следующая запись:
      | Гречневая крупа ядрица | 308 / 12,6 / 3,3 / 57,1 |


